/* jslint node: true */
/* global angular, sessionStorage */
'use strict';

var App = angular.module('shared.auth', []);

/**
 * Service that checks for authorization in routes resolve
 * @return {object} - angular promise object
 */

App.service('authResolve', ['$rootScope', '$q', '$location', '$cookies', 'Query', '$state',
function($rootScope, $q, $location, $cookies, Query, $state){

	return function(){
		// Set deferred
		var deferred = $q.defer();
		// Try to get auth token from cookies
		var auth_token = $cookies.get('authToken');
    var user = sessionStorage.getItem('user');

		// Authorize user if we have user model in route scope
		if($rootScope.user) resolve();
    // else if(user){
    //   user = JSON.parse(user);
    //   $rootScope.user = user;
    //   resolve();
    // }

		// Unauthorize user if we doesn't have auth_token in cookies
		if(!auth_token) reject();

		/**
		 * Send request to server with auth_token from cookies
		 * authorize on success
		 * unauthorize on error
		 */
		else {
			Query.me.get({
				token: auth_token
			}, resolve, reject);
		}

		/**
		 * Authorize function. Create user model in rootScope
		 * and resolve promise for route resolve
		 * @param  {obj} res - response from server
		 * @return {undefined}
		 */
		function resolve(res) {
			if(res) {
        $rootScope.user = res;
        sessionStorage.setItem('user', JSON.stringify(res));
      }
			deferred.resolve('Authorized');
		}

		/**
		 * Unauthorize function. Remove user model in rootScope
		 * and reject promise for route resolve
		 * @return {undefined}
		 */
		function reject(res) {
			$rootScope.user = null;
			$cookies.remove('authToken');
      sessionStorage.setItem('user', null);
			deferred.resolve('Unauthorized');
			$state.go('login');
		}

		// Return promise
		return deferred.promise;
	};
}]);

/**
 * Service which recieve auth_token from cookies
 * @param  {object} $cookies - angular cookies lib
 * @return {String} - auth_token
 */
App.service('authToken', ['$cookies', function($cookies){
	return function(){
		return $cookies.get('authToken');
	};
}]);

App.service('sCurrentUser', ['$cookies', function($cookies){
	return function(){
    var user = sessionStorage.getItem('user');
    if(user){
      user = JSON.parse(user);
    }
		return user;
	};
}]);

/**
 * Logout from applicaton: send request to server,
 * on success remove auth_token from cookies,
 * remove user model from rootScope
 * and redirect to login route
 * @return {undefined}
 */
App.service('authLogout', ['$rootScope', '$cookies', '$location', 'Query', function($rootScope, $cookies, $location, Query){
	return function(){
		Query.userLogout.post(function(){
			$cookies.authToken = false;
			$rootScope.user = null;
		});
	};
}]);

module.exports = App;
