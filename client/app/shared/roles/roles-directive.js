/* jslint node: true*/
'use strict';

module.exports = function($compile){
	function link(scope, element, attributes) {
		var watcher = scope.$watch('role', function(role){
			element.html('<div static-include="' + role.template + '" ng-controller="' + role.controller + '"></div>');
			$compile(element.contents())(scope);
			watcher();
		});
	}

	return {
		restrict: 'E',
		template: '<div class="view-holder"></div>',
		scope: {
			role: '='
		},
		replace: true,
		link: link
	};
};
