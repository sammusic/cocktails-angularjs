module.exports = angular.module('components.dashboard', [])
  .controller('dashboardController', require('./dashboard-controller.js'))
  .controller('dashboardCocktailViewController', require('./cocktail-view/cocktail-controller.js'))
  .directive('dashboardFilterForm', require('./filter-form/filter-form-directive.js'))
  .config(require('./dashboard-translations'));
