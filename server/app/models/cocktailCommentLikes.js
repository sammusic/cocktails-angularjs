// load the things we need
var mongoose      = require('mongoose');

var cocktailCommentLikeSchema = mongoose.Schema({
    user            : { type: mongoose.Schema.Types.ObjectId, ref: 'User', require: true },
    comment         : { type: mongoose.Schema.Types.ObjectId, ref: 'CocktailComment', require: true },
    status          : { type: Boolean, default: true },
    createdAt       : { type: Date, default: Date.now },
    updatedAt       : { type: Date }
});

module.exports = mongoose.model('CocktailCommentLike', cocktailCommentLikeSchema);
