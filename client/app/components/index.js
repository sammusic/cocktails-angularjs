module.exports = angular.module('components', [
  require('./admin').name,
  require('./dashboard').name,
  require('./about-us').name,
  require('./login').name,
  require('./forgot-password').name,
  require('./forgot-password-validate').name,
  require('./signup').name,
  require('./signup-validate').name,
  require('./user').name
]);
