/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['Config', 'Query', function (Config, Query) {
  function link(scope, element, attr) {
    // initial state
    scope.alerts = [];
    scope.form = {};
    scope.title = 'Create your own bar';
    scope.btnLabel = 'Create';

    if(attr.id){
      scope.id = attr.id;
      scope.btnLabel = 'Update';
      Query.bar.get({id: scope.id}).$promise.then(function(data){
        scope.form = angular.merge(scope.form, data.data);
        scope.title = 'Edit your ' + data.data.title + ' bar';
      });
    }

    // remove alert
    scope.closeAlert = function(index){
      scope.alerts.splice(index, 1);
    };

    scope.ingridients = [];
    scope.getIngridients = function(q){
      q = q || '';
      Query.ingridients.get({q: q}).$promise.then(function(data){
        scope.ingridients = data.data;
      });
    };

    scope.submit = function(){
      if(scope.id){
        Query.bar.update({id: scope.id}, scope.form).$promise.then(function(data){
          scope.form = data.data;
          scope.alerts = [{"type": "success", "message": "Your bar has been created"}];
        });
      }else{
        Query.bar.save(scope.form).$promise.then(function(data){
          scope.form = data.data;
          scope.alerts = [{"type": "success", "message": "The bar has been updated"}];
          scope.title = 'Edit your ' + data.data.title + ' bar';
        });
      }
    };
  }

  return {
    templateUrl: Config.rootPath + 'components/user/bar/form/user-bar-form-view.html',
    link: link,
    replace: true
  };
}];
