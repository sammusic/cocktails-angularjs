/* jslint node: true */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', function (Config, Query, $root) {
  function link(scope) {

  }

  return {
    templateUrl: Config.rootPath + 'components/user/sidebar/user-sidebar-view.html',
    link: link,
    replace: true
  };
}];
