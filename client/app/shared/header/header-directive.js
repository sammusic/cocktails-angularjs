/* jslint node: true */
'use strict';

module.exports = ['Config', '$rootScope', '$translate', '$cookies', '$rootScope', function (Config, $root, $translate, $cookies, $rootScope) {
  function link(scope) {
    scope.navLinks = [
      {title: 'DASHBOARD', href: 'dashboard'},
      {title: 'ABOUTUS', href: 'about-us'},
    ];

    scope.changeLanguage = function () {
      $translate.use(($translate.use() === 'en') ? 'ru' : 'en');
    };

    scope.locales = Config.locales;

    scope.locale = $rootScope.lang;
    $translate.use(scope.locale);

    scope.updateLocale = function (locale) {
      $cookies.put('lang', locale);
      scope.locale = locale;
      $rootScope.lang = locale;
      $translate.use(locale);
    };
  }

  return {
    templateUrl: Config.rootPath + 'shared/header/header-view.html',
    link: link,
    replace: true
  };
}];
