/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', 'fileReader', 'Upload', 'authToken', '$state',
function (Config, Query, $rootScope, fileReader, Upload, authToken, $state) {

  function link(scope, element, attr) {

    // initial state
    scope.alerts = [];
    scope.form = {
      role: 'user'
    };
    scope.title = 'Create new user';
    scope.forgotPassword = {};
    scope.registration = {};

    scope.roles = [
      {value: 'user', label: 'User'},
      {value: 'editor', label: 'Editor'},
      {value: 'admin', label: 'Admin'}
    ];

    if(attr.id){
      scope.id = attr.id;
      Query.adminUser.get({id: scope.id}).$promise.then(function(data){
        scope.form = angular.merge(scope.form, data.data.local);
        scope.form.password = scope.form.passwordVerify = '';
        scope.title = 'Edit ' + scope.form.firstName + ' ' + scope.form.lastName;
      });

      Query.adminUser.forgotpassword({id: scope.id}).$promise.then(function(data){
        scope.forgotPassword = data.data;
      });
      Query.adminUser.registration({id: scope.id}).$promise.then(function(data){
        scope.registration = data.data;
      });
    }

    // submit form
		scope.submit = function(){
      if(scope.form.password.length && scope.form.password !== scope.form.passwordVerify){
        scope.alerts = [{"type": "danger", "message": "Passwords are different!"}];
        return;
      }
      if(scope.form.password.length && scope.form.password.length < 8){
        scope.alerts = [{"type": "danger", "message": "Password is too small. Should be greater than 7 symbols."}];
        return;
      }
      // scope.form.author = $rootScope.user.user._id;
      if(scope.id){
        Query.adminUser.update({id: scope.id}, scope.form).$promise.then(function(res){
          scope.files = [];
          scope.form = res.data.local;
          scope.form.password = scope.form.passwordVerify = '';
          scope.title = 'Edit ' + scope.form.firstName + " " + scope.form.lastName;
          scope.alerts = [{"type": "success", "message": "User "+ scope.form.firstName + " " + scope.form.lastName +" has been updated."}];
        }, function (err) {
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }else{
        Query.adminUser.save(scope.form).$promise.then(function(res){
          scope.form = res.data.local;
          scope.form.password = scope.form.passwordVerify = '';
          scope.id = res.data._id;
          scope.title = 'Edit ' + scope.form.firstName + " " + scope.form.lastName;
          scope.alerts = [{"type": "success", "message": "New user " + scope.form.firstName + " " + scope.form.lastName + " has been created."}];
          $state.go('adminUserEdit', {id: res.data.data._id});
        }, function (err) {
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }
		};

    // remove alert
    scope.closeAlert = function(index){
      scope.alerts.splice(index, 1);
    };

    scope.onDelete = function(){
      Query.adminUser.remove({id: scope.id}).$promise.then(function(){
        $state.go('adminUsers');
      });
    };
  }

  return {
    templateUrl: Config.rootPath + 'components/admin/users/users-form/user-form-view.html',
    link: link,
    replace: true
  };
}];
