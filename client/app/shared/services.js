/* jslint node: true */
/* global angular */
var App = angular.module('shared.services', []);

App.factory('AuthService', function ($http, $rootScope, USER_ROLES, AuthResolver, $q, sCurrentUser) {
  var authService = {};
  authService.isAuthenticated = function () {
    return $rootScope.user ? $rootScope.user.user : false;
  };
  authService.isAuthorized = function (authorizedRoles) {
    var user = sCurrentUser();
    var role = 'guest';
    if(user){
      role = user.user.local.role;
    }
    var access = false;
    if(role != 'guest'){
      authorizedRoles.forEach(function(r){
        if(!access){
          if(r === role){
            access = true;
          }
        }
      });
    }
    return access;
  };

  return authService;
});

App.config(function ($httpProvider) {
  $httpProvider.interceptors.push([
    '$injector',
    function ($injector) {
      return $injector.get('AuthInterceptor');
    }
  ]);
});

App.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized,
        419: AUTH_EVENTS.sessionTimeout,
        440: AUTH_EVENTS.sessionTimeout
      }[response.status], response);

      return $q.reject(response);
    }
  };
});

App.factory('AuthResolver', function ($q, $rootScope, $state) {
  return {
    resolve: function () {
      var deferred = $q.defer();
      var unwatch = $rootScope.$watch('user', function (currentUser) {
        if (angular.isDefined(currentUser)) {
          if (currentUser) {
            deferred.resolve(currentUser);
          } else {
            deferred.reject();
          }
          unwatch();
        }else{
          deferred.reject();
        }
      });
      return deferred.promise;
    }
  };
});

App.factory("fileReader", ["$q", "$log", function ($q, $log) {
    var onLoad = function(reader, deferred, scope) {
        return function (event) {
          var image = new Image();
          image.src = event.target.result;

          image.onload = function() {
              var _this = this;
              // access image size here
              scope.$apply(function () {
                deferred.resolve({
                  result: reader.result,
                  img: _this
                });
              });
          };
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject({
                  result: reader.result,
                  img: null
                });
            });
        };
    };

    var onProgress = function(reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress",
                {
                    total: event.total,
                    loaded: event.loaded
                });
        };
    };

    var getReader = function(deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);

        return deferred.promise;
    };

    return {
        readAsDataUrl: readAsDataURL
    };
}]);

App.service('tModel', ['Config', '$rootScope', function(Config, $rootScope){
  var langsObj = {};
  Config.locales.forEach(function(item){
    langsObj[item.id] = '';
  });

  var srv = {
    // this function add translates to the model object with specific fields
    // and return updated model
    updateModel: function(model, options){
      var newObj = {};

      if(options && options.fields){
        for(var key in options.fields){
          if(!model[key]) model[key] = '';
          newObj = {};
          if(typeof model[key] !== 'object'){
            for(var lang in langsObj){
              newObj[lang] = model[key];
            }
            model[key] = angular.extend(angular.copy(langsObj), newObj);
          }else{
            for(var local in model[key]){
              if(model[key][local] == ''){
                model[key][local] = model[key][$rootScope.lang];
              }
            }
          }
        }
      }else{
        // title
        if(typeof model.title !== 'object'){
          if(!model.title) model.title = '';
          newObj = {};
          for(var lang in langsObj){
            newObj[lang] = model.title;
          }
          model.title = angular.extend(angular.copy(langsObj), newObj);
        }else{
          for(var local in model.title){
            if(model.title[local] == ''){
              model.title[local] = model.title[$rootScope.lang];
            }
          }
        }
        // description
        if(typeof model.description !== 'object'){
          if(!model.description) model.description = '';
          newObj = {};
          for(var lang in langsObj){
            newObj[lang] = model.description;
          }
          model.description = angular.extend(angular.copy(langsObj), newObj);
        }else{
          for(var local in model.description){
            if(model.description[local] == ''){
              model.description[local] = model.description[$rootScope.lang];
            }
          }
        }
      }
      return model;
    },

    setModel: function(){

    }
  };

  return srv;
}]);


module.exports = App;
