var settings = {
  rootPath: 'app/',
  apiPath: 'api/',
  date: {
    format: 'dd-mm-yyyy HH:mm:ss'
  },
  locales: [
    { name: 'EN', id: 'en' },
    { name: 'UA', id: 'ua' },
    { name: 'RU', id: 'ru' },
  ]
};

module.exports = settings;
