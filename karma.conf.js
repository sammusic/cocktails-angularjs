// Karma configuration
// Generated on Mon Jul 21 2014 11:48:34 GMT+0200 (CEST)
module.exports = function(config) {
  config.set({

    // base path used to resolve all patterns (e.g. files, exclude)
    basePath: '',

    plugins: [
      'karma-browserify',
      'karma-mocha',
      'karma-coverage',
      'karma-sinon-chai',
      'karma-phantomjs-launcher'
    ],

    // frameworks to use
    frameworks: ['browserify', 'mocha', 'sinon-chai'],

    // list of files / patterns to load in the browser
    files: [
      // './node_modules/angular/angular.js',
      // './node_modules/angular-mocks/angular-mocks.js',
      // './node_modules/angular-ui-router/release/angular-ui-router.min.js',
      // './client/app.modules.js',
      // './client/app.queries.js',
      './client/build/vendor.js',
      './client/build/app.js',
      './client/**/*.js'
    ],

    // list of files to exclude
    exclude: [],

    // preprocess matching files before serving them to the browser
    preprocessors: {
      './client/build/*.js': ['browserify', 'coverage']
    },

    coverageReporter: {
      type: 'text-summary',
      dir: 'coverage/'
    },

    // test results reporter to use
    reporters: ['progress', 'coverage'],

    // web server port
    port: 9000,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests on file changes
    autoWatch: true,

    // start these browsers
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
