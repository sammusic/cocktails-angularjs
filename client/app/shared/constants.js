/* jslint node: true */
/* global angular */
var App = angular.module('shared.constants', []);

App.constant('USER_ROLES', {
  guest:  ['guest', 'user', 'editor', 'admin'],
  user:   ['user', 'editor', 'admin'],
  editor: ['editor', 'admin'],
  admin:  ['admin']
});

App.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
});

module.exports = App;
