var Ingridient = require('../../../models/ingridient');

var walkDir = require('../tools/walkDir');
var sLocale = require('../../options/locales');

var App = {
  get: function(req, res, next){
    Ingridient.find({}, function(err, data){
      if(err) return res.status(500).json(err);
      next(null, JSON.stringify(data));
    });
  },

  apply: function(data){
    data.forEach(function(item){
      Ingridient.findOne({_id: item._id}, function(err, data){
        if(!data){
          new Ingridient(item).save();
        }else{
          var importedTime = new Date(item.updatedAt);
          var inDBTime = new Date(data.updatedAt);
          if(importedTime > inDBTime){
            // TODO: only replace this params
            // NEED TO BE AUTO see users-migrate
            sLocale.compare(data.title, item.title);
            sLocale.compare(data.description, item.description);
            data.categories = item.categories;
            data.images = item.images;
            data.author = item.author;
            data.visibility = item.visibility;
            data.updatedAt = new Date();

            data.markModified('title');
            data.markModified('description');
            data.markModified('categories');
            data.markModified('updatedAt');
            data.save();
          }
        }
      });
    });
  },

  getImages: function(req, res, next){
    var files = walkDir('./uploads/ingridients');
    next(null, files);
  },
};

module.exports = App;
