
module.exports = {
  COCKTAILS_NOT_FOUND: 'No cocktails in the database',
  DASHBOARD_SEARCH_CATEGORIES: 'Select categories...',
  DASHBOARD_SEARCH_INGRIDIENTS: 'Select ingridients...',
  DASHBOARD_SEARCH_BY_TITLE: 'Search by title',
  DASHBOARD_CLEAR: 'Clear',

  AUTHOR: 'Author',
  CATEGORIES: 'Categories',
  INGRIDIENTS: 'Ingridients'
};
