var Cocktail = require('../../models/cocktail');
var sLocale = require('../options/locales');

var DashboardRoutes = {
  dashboard: function (req, res) {
    var cats = req.query.categories || [];
    var ings = req.query.ingridients || [];
    var query = Cocktail.find({});
    var q = req.query.q || '';

    if(cats.length){
      if(typeof cats == 'string'){
        cats = [cats];
      }
      query.where('categories').in(cats);
    }
    if(ings.length){
      if(typeof ings == 'string'){
        ings = [ings];
      }
      query.where('ingridients.ing').in(ings);
    }

    if(q){
      var regex = new RegExp(q, 'i');
      query.where({$or: sLocale.createSearchModel([], ['title'], regex)});
    }

    query.skip(+req.query.offset);
    query.limit(+req.query.limit);
    query.sort({'updatedAt': -1});
    // query.populate('author');
    var populatedFields = Cocktail.getPopulatedFields();

    query.exec(function (err, data) {
      if(err) return res.status(500).json(err);
      Cocktail.populate(data, populatedFields, function (err, data) {
        if(err) return res.status(500).json(err);
        return res.status(200).send({data: data});
      });
    });
  },
};


module.exports = DashboardRoutes;
