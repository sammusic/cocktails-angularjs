/* jslint node: true*/

'use strict';

module.exports = ['$routeProvider', 'Config', function ($routeProvider, Config) {

  var generalResolve = {
		auth: ['authResolve', function(authResolve){
      console.log(authResolve());
			return authResolve();
		}]
	};

  $routeProvider
    .when('/dashboard', {
      templateUrl: Config.rootPath + 'components/dashboard/dashboard-view.html',
      controller: 'dashboard',
      resolve: generalResolve
    })
    .when('/login', {
      templateUrl: Config.rootPath + 'components/login/login-view.html',
      controller: 'login'
    })
    .when('/user', {
      templateUrl: Config.rootPath + 'components/user/home/user-view.html',
      controller: 'userHome',
      resolve: generalResolve
    })
    .when('/user/messages', {
      templateUrl: Config.rootPath + 'components/user/messages/user-messages-view.html',
      controller: 'userMessages',
      resolve: generalResolve
    })
    .when('/user/settings', {
      templateUrl: Config.rootPath + 'components/user/settings/user-settings-view.html',
      controller: 'userSettings',
      resolve: generalResolve
    })

    .when('/cocktail/create', {
      templateUrl: Config.rootPath + 'components/cocktail/cocktail-create/cocktail-create-view.html',
      controller: 'cocktailView',
      // resolve: generalResolve
    })
    .when('/cocktail/:id', {
      templateUrl: Config.rootPath + 'components/cocktail/cocktail-view.html',
      controller: 'cocktailView',
      // resolve: generalResolve
    })
    .otherwise({
      redirectTo: '/dashboard'
    });
}];
