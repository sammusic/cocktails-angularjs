var sort = require('./sort-middleware');

module.exports = {
  list: function(req, res, next) {
    sort(req, res);
    next();
  }
};
