module.exports = angular.module('components.admin.ingridientCategory', [])
  .controller('ingCategoryListController', require('./ingCategory-controller.js'))
  .controller('ingCategoryViewController', require('./view/ingCategory-controller.js'))
  .controller('ingCategoryCreateController', require('./create/ingCategory-create-controller.js'))
  .controller('ingCategoryEditController', require('./edit/ingCategory-edit-controller.js'))

  .directive('ingridientCategoryForm', require('./form/ingCategory-form-directive.js') )
;
