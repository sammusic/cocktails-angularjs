/* jslint node: true */
/* global angular */
'use strict';

var App = angular.module('shared.filters', []);

App.filter('sitedateformat', function(Config) {
    return function(input) {
    return new Date(input).format(Config.date.format);
  };
});

module.exports = App;
