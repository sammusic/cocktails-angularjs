/* jslint node: true */
/* global sessionStorage */
'use strict';

module.exports = ['$scope', 'Query', function ($scope, Query) {

  $scope.form = {
    email: ''
  };

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.submit = function(){
    $scope.form.url = window.location.host;
    Query.userForgotPassword.post($scope.form).$promise.then(function(res){
      $scope.alerts = [{type: 'success', msg: 'Recovered data has been sent.'}];
    }, function(err){
      $scope.alerts = [{type: 'danger', msg: err.data.message}];
    });
  };
}];
