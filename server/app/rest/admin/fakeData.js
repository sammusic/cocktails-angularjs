var Cocktail = require('../../models/cocktail');
var User = require('../../models/user');

var Test = {
  createAdmin: function(req, res){
    User.findOne({'local.email': 'admin@admin.com'}, function(err, user){
      if(!user){
        var pass = 12345;
        var test = new User({
          local: {
            email: 'admin@admin.com',
            password: pass,
            firstName: 'Admin',
            lastName: 'Adminich',
            role: 'admin',
            status: 3,
            createdAt: new Date(),
            updatedAt: null,
            lastLoginAt: null
          }
        });
        test.generateHash(pass);
        test.save(function(err){
          if(err) res.status(500).send({'message': 'something wrong', 'error': err.toString()});
          return res.send(200, {'message': 'admin has been created'});
        });
      }else{
        return res.status(200).send({message: 'admin already exist'});
      }
    });
  },
  
  fakeData: function (req, res) {
    User.findOne({"local.email": "admin@admin.com"}, function(err, user){
      if(err) res.status(400).send({"message": err});
      var test = new Cocktail({
          title: 'cocktail 1',
          description: 'description about something',
          author: user._id
      });
      test.save(function(err, data){});
      test = new Cocktail({
          title: 'B 52',
          description: 'short cocktail for something',
          author: user._id
      });
      test.save(function(err, data){});
      test = new Cocktail({
          title: 'Long Island Ice Tea',
          description: 'this is a long cocktail for long time edition',
          author: user._id
      });
      test.save(function(err, data){});
      test = new Cocktail({
          title: 'New title of cocktail',
          description: 'lorem ipusm lorem ipusm lorem ipusm lorem ipusm lorem ipusm lorem ipusm ',
          author: user._id
      });
      test.save(function(){});
      test = new Cocktail({
          title: 'one more title',
          description: '-- lorem ipusm lorem ipusm lorem ipusm lorem ipusm lorem ipusm lorem ipusm ',
          author: user._id
      });
      test.save(function(err, data){});

      return res.status(200).send({"message": "data has been created"});
    });
  },
};


module.exports = Test;
