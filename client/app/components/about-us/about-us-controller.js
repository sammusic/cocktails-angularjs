/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['$scope', 'Query', function ($scope, Query) {
  $scope.$on('$viewContentLoaded', function(event){
    $scope.htmlReady();
  });
}];
