module.exports = angular.module('shared.roles', [])
.directive('roleContainer', require('./roles-directive.js'))
.directive('staticInclude', require('./roles-static-directive.js'));
