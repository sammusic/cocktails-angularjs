
module.exports = {
  SIGNUPFORM: 'Форма Регистрации',
  FIRSTNAMEFIELD: 'Имя',
  LASTNAMEFIELD: 'Фамилия',
  EMAILFIELD: 'Почта',
  EMAILVERIFYFIELD: 'Почта еще раз',
  PASSWORDFIELD: 'Пароль',
  PASSWORDVERIFYFIELD: 'Пароль еще раз',
  SAVEBUTTON: 'Зарегистрировать',
  RESETBUTTON: 'Очистить поля',
};
