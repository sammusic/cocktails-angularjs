var User = require('../../models/user');

// 0 - blocked, 1 - waiting request, 2 - sent request, 3 - approved
var STATUS_BLOCKED = 0;
var STATUS_REQUEST = 1;
var STATUS_WAITING = 2;
var STATUS_APPROVED = 3;

var sUserFriend = {
  create: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).json({"message": "Bad request. No id parameter."});

    // check this id in the your friends list
    User.findOne({"_id": req.user._id, "friends.id": id}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(user) return res.status(400).json({message: "This friend already exists in the your friend list."});

      // check you in the his friend list (not necessary but for garantly!)
      var check = false;
      var blocked = false;
      User.findOne({"_id": id}, function(err, another_user) {
        if(err) return res.status(500).json(err);
        // get status
        another_user.friends.forEach(function(item, key){
          if(item.id + '' == req.user._id + ''){
            check = true;
            switch(item.status){
              case STATUS_BLOCKED: blocked = true; break;
              case STATUS_REQUEST:
              case STATUS_WAITING:
                another_user.friends[key].status = STATUS_APPROVED; // not necessary break!
              case STATUS_APPROVED:
                var group = req.body.group || '';
                req.user.friends.push({
                  id: id,
                  status: STATUS_APPROVED,
                  group: group
                });
              break;
            }
          }
        });

        if(blocked){
          return res.status(400).json({message: "This user has been blocked your!"});
        }

        if(!check){
          var group = req.body.group || '';
          req.user.friends.push({
            id: id,
            status: STATUS_WAITING,
            group: group
          });

          another_user.friends.push({
            id: req.user._id,
            status: STATUS_REQUEST,
          });
        }
        req.user.save(function(err, obj){
          if(err) return res.status(500).json(err);
          another_user.save(function(err, obj2){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        });
      });
    });
  },

  edit: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": req.user._id, "friends.id": id, "friends.status": STATUS_APPROVED}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "This friend is not found."});

      user.friends.forEach(function(item, index){
        if(id == item.id){
          var group = req.body.group || '';
          user.friends[trueIndex].group = group;
          user.save(function(err, obj){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        }
      });
    });
  },

  delete: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).json({"message": "Bad request. No id parameter."});

    User.findOne({"_id": req.user._id, "friends.id": id}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "This friend is not found."});

      user.friends.forEach(function(item, index){
        if(id == (item.id + '')){
          user.friends.splice(index, 1);
        }
      });

      User.findOne({"_id": id}, function(err, another_user) {
        if(err) return res.status(500).json(err);

        another_user.friends.forEach(function(item, index){
          if(req.user._id == (item.id + '')){
            another_user.friends[index].status = STATUS_WAITING;
          }
        });

        user.save(function(err, obj){
          if(err) return res.status(500).json(err);
          another_user.save(function(err, obj2){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        });
      });
    });
  },

  approve: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).json({"message": "Bad request. No id parameter."});

    User.findOne({"_id": req.user._id, "friends.id": id, "friends.status": STATUS_REQUEST}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "This friend is not found."});

      User.findOne({"_id": id, "friends.id": req.user._id, "friends.status": STATUS_WAITING}, function(err, another_user) {
        if(err) return res.status(500).json(err);
        if(!another_user) return res.status(400).json({message: "This friend is not found."});

        user.friends.forEach(function(item, index){
          if(id == (item.id + '')){
            user.friends[index].status = STATUS_APPROVED; // APPROVED
          }
        });

        another_user.friends.forEach(function(item, index){
          if(req.user._id == (item.id + '')){
            another_user.friends[index].status = STATUS_APPROVED; // APPROVED
          }
        });

        user.save(function(err, obj){
          if(err) return res.status(500).json(err);
          another_user.save(function(err, obj2){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        });
      });
    });
  },

  decline: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).json({"message": "Bad request. No id parameter."});

    User.findOne({"_id": req.user._id, "friends.id": id, "friends.status": STATUS_REQUEST}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "This friend is not found."});

      User.findOne({"_id": id, "friends.id": req.user._id, "friends.status": STATUS_WAITING}, function(err, another_user) {
        if(err) return res.status(500).json(err);
        if(!another_user) return res.status(400).json({message: "This friend is not found."});

        user.friends.forEach(function(item, index){
          if(id == (item.id + '')){
            user.friends.splice(index, 1);
          }
        });

        another_user.friends.forEach(function(item, index){
          if(req.user._id == (item.id + '')){
            another_user.friends.splice(index, 1);
          }
        });

        user.save(function(err, obj){
          if(err) return res.status(500).json(err);
          another_user.save(function(err, obj2){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        });
      });
    });
  },

  blocked: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).json({"message": "Bad request. No id parameter."});

    User.findOne({"_id": req.user._id}, function(err, user) {
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "This user can not be found."});

      var checked = false;
      user.friends.forEach(function(item, index){
        if(id == (item.id + '')){
          checked = true;
          user.friends[index].status = STATUS_BLOCKED;
        }
      });
      if(!checked){
        user.friends.push({
          id: id,
          status: STATUS_BLOCKED
        });
      }

      User.findOne({"_id": id}, function(err, another_user) {
        if(err) return res.status(500).json(err);
        if(!another_user) return res.status(400).json({message: "This user can not be found."});

        another_user.friends.forEach(function(item, index){
          if(req.user._id == (item.id + '')){
            another_user.friends.splice(index, 1);
          }
        });

        user.save(function(err, obj){
          if(err) return res.status(500).json(err);
          another_user.save(function(err, obj2){
            if(err) return res.status(500).json(err);
            return res.status(200).json({data: obj});
          });
        });
      });
    });
  },
};

module.exports = sUserFriend;
