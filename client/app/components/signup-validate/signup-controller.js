/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', '$state', '$stateParams', '$timeout', function ($scope, Query, $state, $stateParams, $timeout) {

  $scope.message = 'We are trying to verify your account! Please wait a couple seconds...';
  if(!$stateParams.code){
    $scope.message = 'Oops, something wrong!';
    $timeout(function(){
      $state.go('login');
    }, 3000);
  }

  Query.userSignUpValidate.post({code: $stateParams.code}).$promise.then(function(){
    $scope.message = 'Success! Please wait, you currently redirect to login!';
    $timeout(function(){
      $state.go('login');
    }, 1000);
  }, function(error){
    $scope.message = 'Something wrong... Please contact with administration. Error: ' + error.message;
    $timeout(function(){
      $state.go('login');
    }, 3000);
  });
}];
