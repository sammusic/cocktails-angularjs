var mongoose      = require('mongoose');
var mongooseI18n = require('mongoose-i18n-localize');
var config = require('../../config/database.js');

// define the schema for our cocktail category model
var ingridientCategorySchema = mongoose.Schema({
    title           : { type: String, required: true, i18n: true},
    description     : { type: String, i18n: true},
});

ingridientCategorySchema.plugin(mongooseI18n, {
	locales: config.locales
});

// ingridientCategorySchema.pre('save', function (next) {
  // if (this.title.length < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  // next();
// });

module.exports = mongoose.model('IngridientCategory', ingridientCategorySchema);
