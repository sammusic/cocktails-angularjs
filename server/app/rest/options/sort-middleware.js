module.exports = function(req, res) {
  req.sort = {};
  if(req.query.sort && req.query.dest){
    req.sort[req.query.sort] = req.query.dest;
  }else{
    req.sort = {'updatedAt': -1};
  }
};
