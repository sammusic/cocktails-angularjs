module.exports = angular.module('components.admin.ingridient', [])
  .controller('ingListController', require('./ing-controller.js'))
  .controller('ingViewController', require('./view/ing-controller.js'))
  .controller('ingCreateController', require('./create/ing-create-controller.js'))
  .controller('ingEditController', require('./edit/ing-edit-controller.js'))

  .directive('ingridientForm', require('./form/ing-form-directive.js') )
  ;
