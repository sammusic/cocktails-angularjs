var nodemailer = require('nodemailer');


var sEmail = {
  init: function(){
    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://mycocktailsbar%40gmail.com:Desktop000!!!@smtp.gmail.com');
    // check verify
    transporter.verify(function(error, success) {
       if (error) {
          // console.log(error);
       } else {
          // console.log('Server is ready to take our messages');
       }
    });

    return transporter;
  },

  signUp: function(user, registration, redirectUrl){
    redirectUrl = redirectUrl || '' ; //'http://localhost:9000/#/signup/verify/';
    var transporter = sEmail.init();

    var dateOffset = (24*60*60*1000) * 3; //5 days
    var myDate = new Date(registration.createdAt);
    myDate.setTime(myDate.getTime() + dateOffset);
    var tempTime = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDay() + ' ' + myDate.getHours() + ':' + myDate.getMinutes() + ':' + myDate.getSeconds();

    var link = 'http://' + redirectUrl + '/#/signup/validate/' + registration.code;

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'MyCocktails <foo@blurdybloop.com>', // sender address
        to: user.local.email, // list of receivers
        subject: 'Welcome to the MyCocktails', // Subject line
        // text: 'Hello world 🐴', // plaintext body
        html: '<b>For apply you registration please go by this link: <a href="'+ link +'" target="_blank">Apply registration</a></b><br><br>'
          + 'Or copy/paste it manually: ' + link + '<br><br>'
          + '<b>This link is temporary and will worked till ' + tempTime + '.</b>' // html body
          + '<b>Thanks.</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log('EMAIL ERROR!', error);
        }
    });
  },

  signUpVerified: function(user){
    var transporter = sEmail.init();

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'MyCocktails <foo@blurdybloop.com>', // sender address
        to: user.local.email, // list of receivers
        subject: 'Registration completed', // Subject line
        // text: 'Hello world 🐴', // plaintext body
        html: '<b>Thanks you for your registration</b>'
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log('EMAIL ERROR!', error);
        }
    });
  },

  forgotPassword: function(user, forgotpassword, recoveredLink){
    var transporter = sEmail.init();

    var link = 'http://' + recoveredLink + '/#/recover-password/' + forgotpassword.code;

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'MyCocktails <foo@blurdybloop.com>', // sender address
        to: user.local.email, // list of receivers
        subject: 'Recovered data', // Subject line
        html: '<b>Please go by this link: <a href="' + link + '">Link</a></b><br><br>'
          + '<b>or copy/paste : ' + link + '</b>'
          + '<b>Thanks</b>'
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log('EMAIL ERROR!', error);
        }
    });
  },

  forgotPasswordSuccess: function(user, forgotpassword){
    var transporter = sEmail.init();

    var link = 'http://localhost:9000/#/recover-password/code';

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'MyCocktails <foo@blurdybloop.com>', // sender address
        to: user.local.email, // list of receivers
        subject: 'Recovered data successed', // Subject line
        html: '<b>You can try login by your new data!</b>'
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
          console.log('EMAIL ERROR!', error);
        }
    });
  }
};

module.exports = sEmail;
