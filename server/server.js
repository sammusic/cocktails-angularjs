// var express  = require('express');
// var app      = express();
var cors  = require('cors');

var app = require('express.io')();
app.http().io();
var path     = require("path");
var port     = process.env.PORT || 9000;
var mongoose = require('mongoose');
var configDB = require('./config/database.js');

var morgan       = require('morgan'); // logger
var bodyParser   = require('body-parser');
var multipart = require('connect-multiparty');
// var fs = require('fs');
// var multipartMiddleware = multipart();

// configuration ===============================================================
app.use(cors());
mongoose.connect(configDB.url); // connect to our database
app.use(morgan('prod')); // log every request to the console
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multipart());

// app.get('/googlebc72d4c9dc9b24c5.html', function(req, res){
//   fs.readFile('./googlebc72d4c9dc9b24c5.html', function(err, data){
//     if(err) return res.status(500).send(err);
//     res.set('Content-Type', 'text/html');
//     return res.status(200).send(data);
//   });
// });
//app.use(express.static(path.join(__dirname, '../client/')));

// routes ======================================================================
require('./app/routes.js')(app); // load our routes and pass in our app and fully configured passport
require('./config/swagger-init')(app); // init swagger

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
