var Ingridient = require('../../models/ingridient');
var sLocale = require('../options/locales');

var CocktailRoutes = {
  list: function (req, res) {
    var offset = +req.query.offset || 0;
    var limit = +req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');

    Ingridient.count({}, function(err, count){
      if(err) return res.status(500).json(err);
      var query = Ingridient.find({});
      query.where({$or: sLocale.createSearchModel([], ['title', 'description'], regex)});
      query.skip(offset);
      query.limit(limit);
      query.sort(req.sort);
      query.populate('author');
      query.populate('categories');

      query.exec(function (err, data) {
        if(err) return res.status(500).json(err);
        return res.status(200).json({
          data: data,
          count: count
        });
      });
    });
  },
};


module.exports = CocktailRoutes;
