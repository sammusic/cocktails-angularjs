/* jslint node: true */
/* global angular */
module.exports = angular.module('shared', [
  require('./header').name,
  require('./confirmation').name,
  require('./navigation').name,
  require('./roles').name,
  require('./noaccess').name,
  require('./directives.js').name,
  require('./services.js').name,
  require('./filters.js').name,
  require('./constants.js').name,
  require('./auth/auth').name
])
.constant('Config', require('./config'));
