module.exports = angular.module('components.admin.cocktail', [])
  .controller('cocktailListController', require('./cocktail-controller.js'))
  .controller('cocktailViewController', require('./cocktail-view/cocktail-controller.js'))
  .controller('cocktailCreateController', require('./cocktail-create/cocktail-create-controller.js'))
  .controller('cocktailEditController', require('./cocktail-edit/cocktail-edit-controller.js'))

  .directive('cocktailForm', require('./cocktail-form/cocktail-form-directive.js') )
  ;
