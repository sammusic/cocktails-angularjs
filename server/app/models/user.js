// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
    local            : {
        email        : String,
        password     : String,
        // roles: admin, editor, user
        role         : {type: String, default: 'user'},
        firstName    : String,
        lastName     : String,
        phone        : String,
        mobile       : String,
        location     : String,
        status       : {type: Number, default: 2}, // 0 - deleted, 1 - blocked, 2 - waiting registation completed, 3 - active
        createdAt    : {type: Date, default: Date.now },
        updatedAt    : {type: Date},
        lastLoginAt  : {type: Date}
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    friends          : [{
      id             : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
      createdAt      : {type: Date, default: Date.now},
      status         : {type: Number}, // 0 - blocked, 1 - waiting request, 2 - sent request, 3 - approved
      group          : {type: String}
    }],
    blocked          : [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});

// userSchema.pre('save', function (next) {
  //if (this.local.email) return next(new Error('There is very small title. Please type more than 3 symbols'));
  // next();
// });

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    this.local.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
