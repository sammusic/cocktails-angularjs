// load the things we need
var mongoose      = require('mongoose');

var cocktailRankingSchema = mongoose.Schema({
    user            : { type: mongoose.Schema.Types.ObjectId, ref: 'User', require: true },
    cocktail        : { type: mongoose.Schema.Types.ObjectId, ref: 'Cocktail', require: true },
    status          : { type: Boolean, default: true },
    createdAt       : { type: Date, default: Date.now },
    updatedAt       : { type: Date }
});

module.exports = mongoose.model('CocktailRanking', cocktailRankingSchema);
