var Cocktail = require('../../models/cocktail');
var imageThumbs = require('../images/image.js');
var sLocale = require('../options/locales');

var cocktailActions = {
  list: function (req, res) {
    Cocktail.count({}, function( err, count){
      var query = Cocktail.find({});
      var text = req.query.q || '';
      var regex = new RegExp(text, 'i');

      query.where({$or: sLocale.createSearchModel([], ['title'], regex)});
      query.skip(+req.query.offset);
      query.limit(+req.query.limit);
      query.sort(req.sort);
      var populatedFields = Cocktail.getPopulatedFields();
      query.exec(function (err, data) {
        if (err) return res.status(500).json(err);
        Cocktail.populate(data, populatedFields, function (err, data) {
          if(err) return res.status(500).json(err);
          return res.status(200).send({data: data, count: count});
        });
      });
    });
  },

  get: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request."});

    var populatedFields = Cocktail.getPopulatedFields();
    var query = Cocktail.findOne({"_id": id});
    query.exec(function(err, data) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      Cocktail.populate(data, populatedFields, function (err, data) {
        return res.status(200).send({data: data});
      });
    });
  },

  create: function(req, res){
    var data = JSON.parse(req.body.data);
    var cocktail = new Cocktail({
        title: data.title,
        description: data.description,
        categories: data.categories,
        ingridients: data.ingridients,
        author: data.author,
        createdAt: new Date()
    });

    var populatedFields = Cocktail.getPopulatedFields();
    cocktail.save(function(err, newSave){
      if(err) return res.status(500).send({'message': err.toString()});
      imageThumbs.images(req, './uploads/cocktails/' + newSave._id + '/', function(images){
        newSave.images = images;
        newSave.save(function(err, data){
          if(err) return res.status(500).send({'message': err.toString()});
          Cocktail.populate(data, populatedFields, function (err, data) {
            return res.status(200).send({data: data});
          });
        });
      });
    });
  },

  edit: function (req, res) {
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    Cocktail.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      var data = JSON.parse(req.body.data);

      var populatedFields = Cocktail.getPopulatedFields();

      sLocale.compare(obj.title, data.title);
      sLocale.compare(obj.description, data.description);
      obj.categories = [];
      data.categories.forEach(function(item){
        obj.categories.push(item._id);
      });
      obj.ingridients = data.ingridients;
      obj.updatedAt = new Date();

      obj.markModified('title');
      obj.markModified('description');
      obj.markModified('updatedAt');
      imageThumbs.images(req, './uploads/cocktails/' + id + '/', function(images){
        obj.images = obj.images.concat(images);
        obj.save(function(err, data){
          if(err) return res.status(500).send(err);
          Cocktail.populate(data, populatedFields, function (err, data) {
            return res.status(200).send({data: data});
          });
        });
      });
    });
  },

  remove: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    Cocktail.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send(err);
      obj.remove({"_id": id}, function(err, data){
        if(err) return res.status(500).send(err);

        imageThumbs.remove(req, './uploads/cocktails/' + id + '/', function(){
          return res.status(200).send({"message": "Cocktail has been deleted"});
        });
      });
    });
  },

  removeAll: function(req, res){
    Cocktail.count({}, function(err, count){
      if(err) return res.status(500).send(err);
      Cocktail.remove({}, function(err){
        if(err) return res.status(500).send(err);
        res.status(200).json({message: count + ' cocktails has been removed'});
      });
    });
  }
};


module.exports = cocktailActions;
