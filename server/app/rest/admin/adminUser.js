var User = require('../../models/user');
var UserForgotPassword = require('../../models/user-actions/user-forgotpass');
var UserRegistration = require('../../models/user-actions/user-registrations');
var bcrypt   = require('bcrypt-nodejs');
var jwt = require('jwt-simple');

var sEmail = require('../options/email-service.js');

var AdminUser = {
  users: function(req, res){
    var offset = +req.query.offset || 0;
    var limit = +req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');

    User.count({}, function(err, userCount){
      var query = User.find({});
      // if(text){
        query.where({$or: [
          {'local.firstName': regex},
          {'local.lastName': regex},
          {'local.email': regex},
        ]});
      // }
      // query.where('local.firstName', regex);
      query.skip(offset);
      query.limit(limit);

      query.sort(req.sort);
      query.exec(function (err, data) {
        if(err) return res.status(500).json(err);
        // called when the `query.complete` or `query.error` are called
        // internally
        return res.status(200).json({
          count: userCount,
          data: data
        });
      });
    });
  },
  // get User by id
  user: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      return res.status(200).send({data: obj});
    });
  },
  create: function(req, res){
    User.findOne({"local.email": req.body.email}, function(err, user){
      if(err) return res.status(500).json(err);
      if(user) return res.status(400).json({message: "User with this email already exists."});

      var newUser = new User({
        local: {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password,
          phone: req.body.phone,
          mobile: req.body.mobile,
          location: req.body.location,
          role: req.body.role,
          status: 1, // by default is blocked
          createdAt: new Date()
        }
      });

      newUser.save(function(err, user){
        if(err) return res.status(500).json(err);
        return res.status(200).json({data: user});
      });
    });
  },
  update: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, user){
      if(err) return res.status(500).json(err);
      if(!user) return res.status(400).json({message: "User not found."});

      user.local.firstName = req.body.firstName;
      user.local.lastName = req.body.lastName;
      user.local.email = req.body.email;
      if(req.body.password.length){
        user.local.password = req.body.password;
        user.generateHash(req.body.password);
      }
      user.local.phone = req.body.phone;
      user.local.mobile = req.body.mobile;
      user.local.role = req.body.role;
      user.local.location = req.body.location;
      user.local.updatedAt = new Date();

      user.save(function(err, user){
        if(err) return res.status(500).json(err);
        return res.status(200).json({data: user});
      });
    });
  },
  delete: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      obj.remove({"_id": id}, function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        return res.status(200).send({"message": "User has been deleted"});
      });
    });
  },
  access: function(req, res){
    var id = req.params.id;
    var access = req.body.access;

    if(req.user._id == id){
      return res.status(400).send({"message": "You can\'t blocked yourself."});
    }

    User.findOne({"_id": id}, function(err, user){
      if(err) return res.status(500).send({"message": "Server error"});
      if(!user) return res.status(404).send({"message": "user not found"});

      user.local.status = access; // 3 - active, 1 - blocked
      user.save(function(err){
        if(err) return res.status(500).send({"message": "Server error"});
        return res.status(200).send({"message": "Access Updated"});
      });
    });
  },


  getForgotPassword: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      UserForgotPassword.findOne({user: id}, function(err, data){
        if(err) return res.status(500).send({"message": "Something wrong"});
        return res.status(200).json({data: data});
      });
    });
  },
  getRegistration: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      UserRegistration.findOne({user: id}, function(err, data){
        if(err) return res.status(500).send({"message": "Something wrong"});
        return res.status(200).json({data: data});
      });
    });
  },

};

module.exports = AdminUser;
