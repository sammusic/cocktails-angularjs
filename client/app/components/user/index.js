module.exports = angular.module('components.user', [
  require('./bar').name,
])
  .controller('userHomeController', require('./home/user-controller.js'))
  .controller('userMessagesController', require('./messages/user-messages-controller.js'))
  .controller('userSettingsController', require('./settings/user-settings-controller.js'))

  .controller('userPublicViewController', require('./user-view/user-view-controller.js'))
  // user
  .directive('userHead', require('./user-head/user-head-directive.js'))
  // sidebar
  .directive('userSidebarProfile', require('./sidebar/sidebar-profile/sidebar-profile-directive.js'))
  .directive('userSidebar', require('./sidebar/user-sidebar-directive.js'))
  ;
