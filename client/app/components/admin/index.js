module.exports = angular.module('components.admin', [
  require('./cocktail').name,
  require('./cocktailCategory').name,
  require('./ingridient').name,
  require('./ingridientCategory').name,
  require('./users').name,
  require('./dbmigrate').name
]);
