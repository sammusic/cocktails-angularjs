var CocktailCategory = require('../../models/cocktailCategory');
var sLocale = require('../options/locales');

var CocktailRoutes = {
  list: function (req, res) {
    var offset = req.query.offset || 0;
    var limit = req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');

    var query = CocktailCategory.find({});
    query.where({$or: sLocale.createSearchModel([], ['title'], regex)});
    query.skip(+offset);
    query.limit(+limit);

    query.exec(function (err, data) {
      res.json({data: data});
    });
  },
};


module.exports = CocktailRoutes;
