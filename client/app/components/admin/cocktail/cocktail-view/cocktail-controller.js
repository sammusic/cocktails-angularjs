/* jslint node: true */
'use strict';

module.exports = ['$scope', 'Query', '$stateParams', function ($scope, Query, $stateParams) {

  $scope.cocktail = {};
  Query.adminCocktail.get({id: $stateParams.id}).$promise.then(function(data){
    $scope.cocktail = data.data;
  });

}];
