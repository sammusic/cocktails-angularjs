/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', '$rootScope', '$stateParams', function ($scope, Query, $rootScope, $stateParams) {
  $scope.user = {};
  $scope.alerts = [];

  // -1 = nothing
  // 0 = blocked
  // 1 = request
  // 2 = waiting
  // 3 = approve
  $scope.addToFriendStatus = -1;

  // remove alert
  $scope.closeAlert = function(index){
    $scope.alerts.splice(index, 1);
  };

  $rootScope.user.user.friends.forEach(function(user){
    if(user.id._id === $stateParams.id){
      $scope.addToFriendStatus = user.status;
    }
  });

  Query.userView.get({id: $stateParams.id}).$promise.then(function(data){
    $scope.user = data.data;
  }, function(err){
    $scope.alerts = [{"type": "danger", "message": err.data.message}];
  });

  $scope.onAddToFriendList = function(){
    Query.userFriend.save({id: $stateParams.id}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": "You sent the request to the " + $scope.user.local.firstName + " " + $scope.user.local.lastName}];
      $scope.addToFriendStatus = 2;
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

  $scope.onApproveFriend = function(){
    Query.userFriend.approve({id: $stateParams.id}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": "User has been added to your friend list"}];
      $scope.addToFriendStatus = 3;
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

  $scope.onDeclineFriend = function(){
    Query.userFriend.decline({id: $stateParams.id}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": "Your declined this request"}];
      $scope.addToFriendStatus = -1;
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

  $scope.onBlockedFriend = function(){
    Query.userFriend.blocked({id: $stateParams.id}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": "Your blocked this user"}];
      $scope.addToFriendStatus = 0;
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

  $scope.onRemoveFromFriendList = function(){
    Query.userFriend.delete({id: $stateParams.id}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": "User has been removed from your friend list"}];
      $scope.addToFriendStatus = -1;
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

}];
