/* jslint node: true*/
/* global angular */
'use strict';

module.exports = ['UIRouterMetatagsProvider', function(UIRouterMetatagsProvider) {
  UIRouterMetatagsProvider
          .setTitlePrefix('')
          .setTitleSuffix(' | Cocktails')
          .setDefaultTitle('Cocktails')
          .setDefaultDescription('The cocktails list.')
          .setDefaultKeywords('cocktails, bar, short, long')
          .setStaticProperties({
                  'fb:app_id': 'your fb app id',
                  'og:site_name': 'your site name'
              })
          .setOGURL(true);
}];
