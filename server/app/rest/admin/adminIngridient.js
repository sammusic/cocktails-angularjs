var Ingridient = require('../../models/ingridient');
var imageThumbs = require('../images/image.js');
var sLocale = require('../options/locales');

var ingridientObj = {

  list: function (req, res) {
    var offset = +req.query.offset || 0;
    var limit = +req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');

    var populateFields = Ingridient.getPopulatedFields();

    Ingridient.count({}, function(err, count){
      if(err) return res.status(500).json(err);
      var query = Ingridient.find({});
      query.where({$or: sLocale.createSearchModel([], ['title', 'description'], regex)});
      query.skip(offset);
      query.limit(limit);
      query.sort(req.sort);
      query.populate(populateFields);

      query.exec(function (err, data) {
        if(err) return res.status(500).json(err);
        return res.status(200).json({
          data: data,
          count: count
        });
      });
    });
  },

  get: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request."});

    var query = Ingridient.findOne({"_id": id});
    query.populate('categories');
    query.exec(function(err, data) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      return res.status(200).send({data: data});
    });
  },

  create: function(req, res){
    var data = JSON.parse(req.body.data);

    var ingridient = new Ingridient({
        title: data.title,
        description: data.description,
        categories: data.categories,
        author: data.author,
        createdAt: new Date()
    });

    var populateFields = Ingridient.getPopulatedFields();

    ingridient.save(function(err, data){
      if(err) return res.status(500).send(err);
      imageThumbs.images(req, './uploads/ingridients/' + data._id + '/', function(images){
        ingridient.images = images;
        ingridient.save(function(err, data){
          if(err) return res.status(500).send({'message': err.toString()});
          Ingridient.populate(data, populateFields, function (err, data) {
            return res.status(200).send({data: data});
          });
        });
      });
    });
  },

  edit: function (req, res) {
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    Ingridient.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      var populateFields = Ingridient.getPopulatedFields();
      var data = JSON.parse(req.body.data);

      sLocale.compare(obj.title, data.title);
      sLocale.compare(obj.description, data.description);
      obj.categories = data.categories;
      obj.images = data.images;
      obj.updatedAt = new Date();

      obj.markModified('title');
      obj.markModified('description');
      obj.markModified('updatedAt');
      imageThumbs.images(req, './uploads/ingridients/' + id + '/', function(images){
        obj.images = obj.images.concat(images);
        obj.save(function(err, data){
          if(err) return res.status(500).send({'message': err.toString()});
          Ingridient.populate(data, populateFields, function (err, data) {
            return res.status(200).send({data: data});
          });
        });
      });
    });
  },

  remove: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    Ingridient.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      obj.remove({"_id": id}, function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        return res.status(200).send({"message": "Ingridient has been deleted"});
      });
    });
  },

  removeAll: function(req, res){
    Ingridient.count({}, function(err, count){
      if(err) return res.status(500).send(err);
      Ingridient.remove({}, function(err){
        if(err) return res.status(500).send(err);
        res.status(200).json({message: count + ' ingridients has been removed'});
      });
    });
  }
};


module.exports = ingridientObj;
