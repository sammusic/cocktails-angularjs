module.exports = angular.module('components.user.bar', [])

  .controller('userBarControllerView', require('./view/user-bar-controller.js'))
  .controller('userBarControllerCreate', require('./create/user-bar-controller.js'))
  .controller('userBarControllerEdit', require('./edit/user-bar-controller.js'))

  .directive('userBarForm', require('./form/user-bar-form-directive.js') )
  ;
