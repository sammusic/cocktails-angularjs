/* jslint node: true*/
/* global angular */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', 'fileReader', 'Upload', 'authToken', '$state', 'tModel',
function (Config, Query, $rootScope, fileReader, Upload, authToken, $state, tModel) {

  function link(scope, element, attr) {

    // initial state
    scope.alerts = [];
    scope.form = tModel.updateModel({});
    scope.title = 'Create new category for cocktail';

    if(attr.id){
      scope.id = attr.id;
      Query.adminCocktailCategory.get({id: scope.id}).$promise.then(function(data){
        scope.form = angular.merge(scope.form, data.data);
        scope.title = 'Edit ' + scope.form.title[$rootScope.lang] + ' cocktail category';
      });
    }

    // submit form
		scope.submit = function(){
      scope.form = tModel.updateModel(scope.form);
      if(scope.id){
        Query.adminCocktailCategory.update({id: scope.id}, scope.form).$promise.then(function(data){
          scope.title = 'Edit ' + data.data.title[$rootScope.lang] + ' cocktail';
          scope.alerts = [{"type": "success", "message": "Cocktail category has been updated."}];
        }, function(err){
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }else{
        Query.adminCocktailCategory.save(scope.form).$promise.then(function(data){
          scope.id = data._id;
          scope.alerts = [{"type": "success", "message": "New cocktail category has been created."}];
          $state.go('adminCocktailCategoryEdit', {
            id: scope.id
          });
        }, function(err){
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }
		};

    // remove alert
    scope.closeAlert = function(index){
      scope.alerts.splice(index, 1);
    };
  }

  return {
    templateUrl: Config.rootPath + 'components/admin/cocktailCategory/form/cocktailCategory-form-view.html',
    link: link,
    replace: true
  };
}];
