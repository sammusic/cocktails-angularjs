var User = require('../models/user');

var sUser = {
  // get User by id
  get: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    User.findOne({"_id": id}, function(err, obj) {
      if(err) return res.status(500).send(err);
      return res.status(200).send({data: obj});
    });
  },
};

module.exports = sUser;
