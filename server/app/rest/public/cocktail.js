var Cocktail = require('../../models/cocktail');
var sLocale = require('../options/locales');

var CocktailRoutes = {
  get: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request."});
    var populatedFields = Cocktail.getPopulatedFields();
    var query = Cocktail.findOne({"_id": id});
    query.exec(function(err, data) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      Cocktail.populate(data, populatedFields, function (err, data) {
        return res.status(200).send({data: data});
      });
    });
  },
};


module.exports = CocktailRoutes;
