/* jslint node: true */
'use strict';

module.exports = ['$scope', 'Query', 'Config', function ($scope, Query, Config) {
  /* Default parameters */
  $scope.offset = 0;
  $scope.limit = 10;
  $scope.sort = {
    column: 'updatedAt',
    dest: -1
  };
  $scope.users = [];
  $scope.total = 0;
  $scope.currentPage = 1;
  $scope.q = '';
  $scope.format = Config.date.format;
  $scope.alerts = [];

  $scope.userAccess = function(access){
    switch(access){
      case 0: return {"className": "btn btn-sm btn-warning","name": "restore"};
      case 1: return {"className": "btn btn-sm btn-warning","name": "access"};
      case 2: return {"className": "btn btn-sm btn-info", "name": "approve registration"};
      case 3: return {"className": "btn btn-sm btn-info", "name": "block"};

    }
  };

  /* Base functionality */
  $scope.tableUpdate = function(){
    Query.adminUsers.get({
      limit: $scope.limit,
      offset: $scope.offset,
      sort: $scope.sort.column,
      dest: $scope.sort.dest,
      q: $scope.q
    }).$promise.then(function(data){
      $scope.total = data.count;
      $scope.users = data.data;
    });
  };

  /* Sort function */
  $scope.doSort = function(column){
    if($scope.sort.column == column){
      $scope.sort.dest = $scope.sort.dest == -1 ? 1 : -1;
    }else{
      $scope.sort.column = column;
    }
    $scope.tableUpdate();
  };

  $scope.onUserAccess = function(user){
    var status = 1;
    console.log(user.local.status);
    switch(user.local.status){
      case 0: status = 3; break;
      case 1: status = 3; break;
      case 2: status = 3; break;
      case 3: status = 1; break;
      default: status = 1;
    }
    // var status = user.local.status ? false : true;
    Query.adminUser.access({id: user._id}, {access: status}).$promise.then(function(data){
      $scope.alerts = [{type: "success", message: "Change access for user <" + user.local.firstName + " " + user.local.lastName + ">"}];
      $scope.tableUpdate();
    }, function(err){
      $scope.alerts = [{type: "warning", message: err.data.message}];
    });
  };

  /* Delete function */
  $scope.onDelete = function(user){
    Query.adminUser.remove({id: user._id}).$promise.then(function(data){
      $scope.tableUpdate();
    });
  };

  /* close alerts */
  $scope.closeAlert = function(index){
    $scope.alerts.splice(index, 1);
  };

  /* Watchers */
  $scope.$watch('q', function(text){
    $scope.tableUpdate();
  });

  /* Init default parameters */
  $scope.tableUpdate();
}];
