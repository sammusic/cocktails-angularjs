// load the things we need
var mongoose      = require('mongoose');
var userSchema    = require('./user');
var mongooseI18n = require('mongoose-i18n-localize');
var config = require('../../config/database.js');

// define the schema for our cocktail model
var cocktailSchema = mongoose.Schema({
    title           : { type: String, required: true, i18n: true},
    description     : { type: String, i18n: true},
    /* each cocktail has many categories */
    categories      : [{ type: mongoose.Schema.Types.ObjectId, ref: 'CocktailCategory'}],
    /* many ingridients with description */
    ingridients     : [
      {
        ing         : { type: mongoose.Schema.Types.ObjectId, ref: 'Ingridient'},
        description : { type: String},
      }
    ],
    /* each cocktail has many images
     in the future we need to limit of images. for example 10 images!
     maybe we need to add some slice procedure in the req.files array
     */
    images          : [],
    author          : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    createdAt       : { type: Date, default: Date.now},
    updatedAt       : { type: Date},

    /* status
     I think about some statuses of Cocktail
     only admin or editor can edit this status
     also each status has different view and speacial features
     but now it's not necessary
     for example:
     0 - basic
     1 - bronze
     2 - silver
     3 - gold
     */
    //status          : Number,
    /* visibility for searching.
     true - this cocktail is searchable,
     false - not searchable
     */
    visibility      : { type: Boolean, default: true},
    /* flag for comments
     maybe author of this cocktail can disable the comment for cocktail
     */
    //canComment      : Boolean,
    /* add commentaries */
    // commentaries    : [{
    //   author        : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    //   comment       : String,
    //   status        : Boolean
    // }]
});

cocktailSchema.statics.getPopulatedFields = function(){
  return [
    {
      path: 'ingridients.ing',
      model: 'Ingridient'
    },
    {
      path: 'categories',
      model: 'CocktailCategory'
    },
    {
      path: 'author',
      model: 'User'
    },
  ];
};

cocktailSchema.plugin(mongooseI18n, {
	locales: config.locales
});

cocktailSchema.pre('save', function (next) {
  if (this.title.length < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  if (this.title.description < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  next();
});

module.exports = mongoose.model('Cocktail', cocktailSchema);
