/* jslint node: true */
/* global angular */
var App = angular.module('shared.directives', []);

App.directive('logout', ['$cookies', '$location', '$rootScope', '$state', function(cookies, location, $root, $stateProvider){
  function link(scope, element) {
    element.bind('click', function(){
      $root.user = null;

      cookies.remove('authToken');
      sessionStorage.removeItem('user');
      location.url('login');
      $stateProvider.go('login');
    });
  }

  return {
    restrict: 'A',
    link: link,
    replace: true
  };

}]);

App.directive('isAuth', function(USER_ROLES, $rootScope){
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      $rootScope.$watch("user", function(value, oldValue) {
        if(!value) {
          attrs.isAuth ? element.addClass('ng-hide') : element.removeClass('ng-hide');
          return false;
        }

        if(value.user.local.role == 'guest'){
          attrs.isAuth ? element.addClass('ng-hide') : element.removeClass('ng-hide');
          return false;
        }

        attrs.isAuth ? element.removeClass('ng-hide') : element.addClass('ng-hide');
      }, true);

      attrs.isAuth ? element.addClass('ng-hide') : element.removeClass('ng-hide');
    }
  };
});

App.directive('showByRole', function(USER_ROLES, $rootScope){
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {

      $rootScope.$watch("user", function(value, oldValue) {
        if(!value) {
            element.addClass('ng-hide');
          return false;
        }

        var access = false;
        if(value.user.local.role === 'guest'){
            element.addClass('ng-hide');
          return false;
        }

        if(value.user.local.role === attrs.showByRole){
          element.removeClass('ng-hide');
          return false;
        }


        if(USER_ROLES[attrs.showByRole]){
          USER_ROLES[attrs.showByRole].forEach(function(r){
            if(!access){
              if(r === value.user.local.role){
                access = true;
              }
            }
          });
        }

        if(access) {
            element.removeClass('ng-hide');
        } else {
            element.addClass('ng-hide');
        }
      }, true);

      element.addClass('ng-hide');
    }
  };
});

App.directive("ngFileSelect",function(){

  return {
    link: function($scope, el){
      el.bind("change", function(e){
        $scope.files = (e.srcElement || e.target).files;
        for(var i = 0, j = $scope.files.length; i < j; i++){
          $scope.getFile($scope.files[i]);
        }
      });
    }
  };
});


App.directive('uiRequired', function() {
    return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {
        ctrl.$validators.required = function(modelValue, viewValue) {
          return !((viewValue && viewValue.length === 0 || false) && attrs.uiRequired === 'true');
        };

        attrs.$observe('uiRequired', function() {
          ctrl.$setValidity('required', !(attrs.uiRequired === 'true' && ctrl.$viewValue && ctrl.$viewValue.length === 0));
        });
      }
    };
  });

  App.directive('uiSelectRequired', function() {
    return {
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        ctrl.$validators.uiSelectRequired = function (modelValue, viewValue) {
             var determineVal;
            if (angular.isArray(modelValue)) {
                determineVal = modelValue;
            } else if (angular.isArray(viewValue)) {
                determineVal = viewValue;
            } else {
                return false;
            }

            return determineVal.length > 0;
        };
    }
  };
});

module.exports = App;
