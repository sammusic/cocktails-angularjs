/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', 'Config', 'Upload', 'authToken', function ($scope, Query, Config, Upload, authToken) {
  $scope.items = [
    {name: 'Ingridient\'s Categories', value: 'ingridients_categories'},
    {name: 'Ingridients', value: 'ingridients'},
    {name: 'Cocktail\'s categories', value: 'cocktails_categories'},
    {name: 'Cocktails', value: 'cocktails'},
    {name: 'Users', value: 'users'},
  ];

  $scope.form = {
    items: {}
  };

  $scope.checkAll = false;
  $scope.$watch('checkAll', function(item){
    if(item){
      $scope.items.forEach(function(item){
        $scope.form.items[item.value] = true;
      });
    }else{
      $scope.form.items = {};
    }
  });

  $scope.onDownload = function(){
    Query.dbmigrate.post($scope.form).$promise.then(function(data){
      // var blob = data.response.blob;
      // var fileName = data.response.fileName || 'document.zip';
      //SaveAs is available at saveAs.js from http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js
      //$window.saveAs(blob, fileName);
    });
  };

  $scope.onUpload = function(){
    Upload.upload({
      url: Config.apiPath + 'dbimport' + '?token='+authToken(),
      method: 'POST',
      file: $scope.files,
      data: $scope.form
    }).then(function (res) {
      $scope.files = [];
    }, function (err) {

    }, function (evt) {
      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
    });
  };

  $scope.uploadFiles = function (files) {
    if(files){
      $scope.files = files;
    }
  };
}];
