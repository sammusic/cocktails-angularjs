module.exports = angular.module('components.admin.users', [])
  .controller('adminUsersController', require('./users-controller.js'))
  .controller('adminUserViewController', require('./user-view/user-view-controller.js'))
  .controller('adminUserCreateController', require('./user-create/user-create-controller.js'))
  .controller('adminUserEditController', require('./user-edit/user-edit-controller.js'))
  .controller('adminUserViewController', require('./user-view/user-view-controller.js'))

  .directive('userForm', require('./users-form/user-form-directive.js') )
  ;
