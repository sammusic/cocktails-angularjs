
module.exports = {
  SIGNINFORM: 'Sign In Form',
  EMAILFIELD: 'Email',
  PASSWORDFIELD: 'Password',
  REMEMBERME: 'Remember me',
  SIGNINBUTTON: 'Sign In',
  FORGOTPASSWORDLINK: 'Forgot password',
};
