// load the things we need
var mongoose      = require('mongoose');

var cocktailCommentSchema = mongoose.Schema({
    user            : { type: mongoose.Schema.Types.ObjectId, ref: 'User', require: true },
    cocktail        : { type: mongoose.Schema.Types.ObjectId, ref: 'Cocktail', require: true },
    status          : { type: Boolean, default: true },
    parent          : { type: mongoose.Schema.Types.ObjectId, ref: 'CocktailComment', default: null },
    comments        : [{ type: mongoose.Schema.Types.ObjectId, ref: 'CocktailComment' }],
    createdAt       : { type: Date, default: Date.now },
    updatedAt       : { type: Date }
});

module.exports = mongoose.model('CocktailComment', cocktailCommentSchema);
