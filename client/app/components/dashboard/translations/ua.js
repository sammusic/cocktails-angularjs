
module.exports = {
  COCKTAILS_NOT_FOUND: 'Коктейлі не знайдені',
  DASHBOARD_SEARCH_CATEGORIES: 'Пошук за категоріями...',
  DASHBOARD_SEARCH_INGRIDIENTS: 'Пошук за інгредієнтами...',
  DASHBOARD_SEARCH_BY_TITLE: 'Пошук за назвою',
  DASHBOARD_CLEAR: 'Очистити',

  AUTHOR: 'Автор',
  CATEGORIES: 'Категорії',
  INGRIDIENTS: 'Інгредієнти'
};
