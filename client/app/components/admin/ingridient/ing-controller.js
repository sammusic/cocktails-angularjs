/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', 'Config', function ($scope, Query, Config) {
  /* Default parameters */
  $scope.offset = 0;
  $scope.limit = 10;
  $scope.sort = {
    column: 'updatedAt',
    dest: -1
  };
  $scope.ingridients = [];
  $scope.total = 0;
  $scope.currentPage = 1;
  $scope.q = '';
  $scope.format = Config.date.format;
  $scope.alerts = [];
  $scope.closeAlert = function(index){
    $scope.alerts.splice(index, 1);
  };

  /* Base functionality */
  $scope.tableUpdate = function(){
    Query.adminIngridients
      .get({
        limit: $scope.limit,
        offset: $scope.offset,
        sort: $scope.sort.column,
        dest: $scope.sort.dest,
        q: $scope.q
      })
      .$promise.then(function(data){
          $scope.ingridients = data.data;
      }, function(err){

    });
  };

  /* Sort function */
  $scope.doSort = function(column){
    if($scope.sort.column == column){
      $scope.sort.dest = $scope.sort.dest == -1 ? 1 : -1;
    }else{
      $scope.sort.column = column;
    }
    $scope.tableUpdate();
  };

  /* Delete function */
  $scope.onIngRemove = function(category){
    Query.adminIngridient.remove({id: category._id}).$promise.then(function(){
      $scope.tableUpdate();
    });
  };

  $scope.onIngridientsRemove = function(){
    Query.adminIngridients.removeAll({}).$promise.then(function(data){
      $scope.alerts = [{"type": "success", "message": data.message}];
      $scope.tableUpdate();
    }, function(err){
      $scope.alerts = [{"type": "danger", "message": err.data.message}];
    });
  };

  /* Watchers */
  $scope.$watch('q', function(text){
    $scope.tableUpdate();
  });

  /* Init default parameters */
  $scope.tableUpdate();
}];
