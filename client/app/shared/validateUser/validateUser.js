/* jslint node: true */
/* global angular */
module.exports = ['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push(myHttpInterceptor);

  var requests = 0;

	function myHttpInterceptor($q) {

		function toggleSpinner() {
			if(!requests) angular.element(document.querySelector('.loader')).removeClass('show');
			else angular.element(document.querySelector('.loader')).addClass('show');
		}

		return {
			request: function(config) {
				// if(config.showErrors) messageHandler('clear', config.showErrors);
				if(config.showLoad) {
					requests++;
					// toggleSpinner();
				}
				return config;
			},
			response: function(response) {
				// if(response.config.successDownload) {
        //     response = downloadHandler(response, response.config.successDownload);
        // }
				if(response.config.showLoad) {
					requests--;
					// toggleSpinner();
				}
				return response;
			},

			responseError: function(rejection){
				// if(rejection.config.showErrors) messageLogin(rejection);
				if(rejection.config.showLoad) {
					requests--;
					// toggleSpinner();
				}
				return $q.reject(rejection);
			}
		};
    }
}];
