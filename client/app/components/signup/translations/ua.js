
module.exports = {
  SIGNUPFORM: 'Форма регістрації',
  FIRSTNAMEFIELD: 'ім\'я',
  LASTNAMEFIELD: 'Прізвище',
  EMAILFIELD: 'Пошта',
  EMAILVERIFYFIELD: 'Пошта ще раз',
  PASSWORDFIELD: 'Пароль',
  PASSWORDVERIFYFIELD: 'Пароль ще раз',
  SAVEBUTTON: 'Зареєструвати',
  RESETBUTTON: 'Очистити поля',
};
