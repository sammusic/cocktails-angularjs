var CocktailCategory = require('../../../models/cocktailCategory');
var sLocale = require('../../options/locales');

var App = {
  get: function(req, res, next){
    CocktailCategory.find({}, function(err, categories){
      if(err) return res.status(500).json(err);
      next(null, JSON.stringify(categories));
    });
  },

  apply: function(data){
    data.forEach(function(item){
      CocktailCategory.findOne({_id: item._id}, function(err, category){
        if(!category){
          new CocktailCategory(item).save();
        }else{
          var importedTime = new Date(item.updatedAt);
          var inDBTime = new Date(category.updatedAt);
          if(importedTime > inDBTime){
            // TODO: only replace this params
            // NEED TO BE AUTO see users-migrate
            sLocale.compare(category.title, item.title);
            sLocale.compare(category.description, item.description);
            category.updatedAt = new Date();

            category.markModified('title');
            category.markModified('description');
            category.markModified('updatedAt');
            category.save();
          }
        }
      });
    });
  },
};

module.exports = App;
