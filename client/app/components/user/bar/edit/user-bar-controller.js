/* jslint node: true*/
'use strict';

module.exports = ['$scope', '$stateParams', function ($scope, $stateParams) {
  $scope.id = $stateParams.id;
}];
