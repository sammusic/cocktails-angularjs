var User = require('../../../models/user');

var App = {
  get: function(req, res, next){
    User.find({}, function(err, users){
      if(err) return res.status(500).json(err);
      next(null, JSON.stringify(users));
    });
  },

  apply: function(data){
    data.forEach(function(item){
      User.findOne({_id: item._id}, function(err, user){
        if(!user){
          new User(item).save();
        }else{
          var importedTime = new Date(item.local.updatedAt);
          var inDBTime = new Date(user.local.updatedAt);
          if(importedTime > inDBTime){
            // TODO: only replace local parametes.
            // NEED TO BE USING : user.schema.paths for all fields
            user.local = item.local;
            user.save();
          }
        }
      });
    });
  }
};

module.exports = App;
