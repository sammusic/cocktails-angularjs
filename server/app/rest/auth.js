var User = require('../models/user');
var UserRegistration = require('../models/user-actions/user-registrations');
var UserForgotPassword = require('../models/user-actions/user-forgotpass');
var Validate = require('./validateRequest');
var bcrypt   = require('bcrypt-nodejs');
var jwt = require('jwt-simple');

var sEmail = require('./options/email-service');

var auth = {

  // Login function
  login: function(req, res) {
    var email = req.body.email || '';
    var pass = req.body.password || '';

    if(!email || !pass) return res.status(400).send({message: "Please fill all fields"});

    User.findOne({'local.email': email}, function(err, user){
      if (err) {
        return res.status(400).send({"message": err});
      }

      if (!user) {
        return res.status(400).send({"message": 'No user found.'});
      }

      if (!user.validPassword(pass)){
        return res.status(400).send({"message": 'Oops! Wrong password.'});
      }

      //0 - deleted, 1 - blocked, 2 - waiting registation completed, 3 - active
      switch(user.local.status){
        case 0: return res.status(400).send({"message": 'Your account has been deleted!'});
        case 1: return res.status(400).send({"message": 'Your account has been blocked!'});
        case 2: return res.status(400).send({"message": 'Your account has been waited to approve registration!'});
      }

      // update last login date
      user.local.lastLoginAt = new Date();
      user.save(function(){
        return res.status(200).send({data: genToken(user)} );
      });
    });
  },
  // forgot-password
  forgotPassword: function(req, res){
    if(!req.body.email) return res.status(400).json({message: "Bad request"});

    User.findOne({"local.email": req.body.email}, function(err, user){
      if(err) return res.status(500).json(err);
      if(!user) return res.status(500).json({message: "User is not found!"});

      //TODO: make check the counter of trying
      UserForgotPassword.findOne({user: user._id}, function(err, forgotObj){
        if(err) return res.status(500).json(err);

        if(forgotObj){
          forgotObj.code = getCode(10);
          forgotObj.createdAt = new Date();
          forgotObj.counter++;
          forgotObj.save(function(err, obj){
            if(err) return res.status(500).json(err);

            sEmail.forgotPassword(user, forgotObj, req.body.url);
            return res.status(200).json({message: "Recovered data has been sent."});
          });
        }else{
          forgotObj = new UserForgotPassword({
            user: user._id,
            code: getCode(10),
          });

          forgotObj.save(function(err, obj){
            if(err) return res.status(500).json(err);

            sEmail.forgotPassword(user, forgotObj, req.body.url);
            return res.status(200).json({message: "Recovered data has been sent."});
          });
        }
      });

      // user.forgotpassword = {
      //   code: getCode(10),
      //   createdAt: new Date()
      // };
      //
      // user.save(function(err, user){
      //   if(err) return res.status(500).json(err);
      //
      //   sEmail.forgotPassword(user, req.body.url);
      //   return res.status(200).json({message: "Recovered data has been sent."});
      // });
    });
  },

  forgotPasswordValidate: function(req, res){
    console.log(req.params);
    if(!req.params.validatecode) return res.status(400).json({message: "Bad request"});

    UserForgotPassword.findOne({"code": req.params.validatecode}, function(err, forgotObj){
      if(err) return res.status(500).json(err);
      if(!forgotObj) return res.status(500).json({message: "Bad request"});

      var dateOffset = (24*60*60*1000) * 1; //1 day
      var userTime = new Date(forgotObj.createdAt).getTime() + dateOffset;
      var currentTime = new Date().getTime();
      if(userTime < currentTime){
        return res.status(400).json({message: "Time for recovered data has been expired!"});
      }

      User.findOne({_id: forgotObj.user}, function(err, user){
        if(err) return res.status(500).json(err);

        if(!user) return res.status(400).json({message: "User not found." + forgotObj.user});

        forgotObj.code = '';
        forgotObj.save(function(err, obj){ });

        user.generateHash(req.body.password);
        user.save(function(err, user){
          if(err) return res.status(500).json(err);

          sEmail.forgotPasswordSuccess(user, forgotObj);
          return res.status(200).json({message: "Recovered data has been sent."});
        });
      });
    });
  },
  // Logout function
  logout: function(req, res) {
    req.logout();
    res.status(200);
  },

  signup: function(req, res){
    var email = req.body.email || '';
    var pass = req.body.password || '';

    if(!email || !pass) return res.status(400).send({message: "Please fill all fields"});

    User.findOne({'local.email': email}, function(err, user){
      if (err) {
        return res.status(400).send({"message": err});
      }

      if (user) {
        return res.status(400).send({"message": 'User with this email already registered.'});
      }

      var newUser = new User({
        local: {
          email: email,
          password: pass,
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          role: 'user',
          status: 2,
          createdAt: new Date(),
          updatedAt: null,
          lastLoginAt: null
        }
      });

      newUser.generateHash(pass);
      newUser.save(function(err, user){
        if(err) return res.status(500).json(err);

        var registration = new UserRegistration({
          user: user._id,
          code: getCode(10)
        });

        registration.save(function(err, obj){
          if(err) return res.status(500).json(err);
          sEmail.signUp(newUser, obj, req.body.url);

          return res.status(201).send({'message': 'New User has been created'});
        });
      });
    });
  },

  // get User model by token
  me: function(req, res){
    res.status(200);
    return res.send({
      token: req.token,
      expires: req.exp,
      user: req.user
    });
  },

  meUpdate: function(req, res){
    req.user.local.firstName = req.body.firstName || '';
    req.user.local.lastName = req.body.lastName || '';
    req.user.local.location = req.body.location || '';
    req.user.local.phone = req.body.phone || '';
    req.user.local.mobile = req.body.mobile || '';
    req.user.local.updatedAt = new Date();

    var email = req.body.email || '';
    var email2 = req.body.email2 || '';
    var password = req.body.password || '';
    var password2 = req.body.password2 || '';

    User.findOne({ $and: [{"_id": {$ne: req.user._id} }, {"local.email": email}] }, function(err, user){
      if(err) return res.status(500).send({"message": "Something wrong!"});

      if(user){
         return res.status(400).send({"message": "This email already exists!"});
      }else{

        // check email
        if(email != req.user.local.email){
          var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/i;
          if(!pattern.test(email)){
            res.status(400).send({"message": "Bad email."});
            return;
          }
          if(email !== email2){
            return res.status(400).send({"message": "Emails are not the same!"});
          }
          req.user.local.email = email;
        }

        // check password
        if(password != ''){
          if(password.length < 8){
            return res.status(400).send({"message": "Too short password. Please type more 7 symbols"});
          }
          if(password !== password2){
            return res.status(400).send({"message": "Passwords are not the same!"});
          }
          req.user.local.password = password;
        }

        req.user.save(function(err, data){
          if(err) return res.status(500).send({"message": "Something wrong!"});
          return res.status(200).send({user: data});
        });
      }
    });
  },
  meDelete: function(req, res){
    req.user.remove({"_id": req.user._id}, function(err, data){
      if(err) return res.status(500).send({'message': err.toString()});
      return res.status(200).send({"message": "User has been deleted"});
    });
  },

  validate: function(req, res){
    UserRegistration.findOne({"code": req.params.validatecode}, function(err, registration){
      if(err) return res.status(500).json(err);
      if(!registration) return res.status(400).json({message: "Something wrong! User can\'t found!"});

      var dateOffset = (24*60*60*1000) * 3; //3 days
      var userDate = new Date(registration.createdAt).getTime() + dateOffset;
      var currentDate = new Date().getTime();
      if(userDate < currentDate){
         return res.status(400).json({message: "Your verify time has been expired! Please try login again and we will send email to your again!"});
      }

      User.findOne({_id: registration.user}, function(err, user){
        if(err) return res.status(500).json(err);
        registration.code = '';
        registration.save(function(err, obj){
          if(err) return res.status(500).json(err);

          user.local.status = 3;
          user.save(function(err, user){
            if(err) return res.status(500).json(err);
            sEmail.signUpVerified(user);
            return res.status(200).json(user);
          });
        });
      });
    });
  }
};

// private method
function genToken(user) {
  var expires = expiresIn(7); // 7 days
  var token = jwt.encode({
    _id: user._id,
    exp: expires
  }, require('../../config/secret')());

  return {
    token: token,
    expires: expires,
    user: user
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
  // return dateObj;
}

function getCode(len){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

module.exports = auth;
