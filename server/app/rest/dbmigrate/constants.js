module.exports = {
  USERS: 'users.json',
  COCKTAILS_CATEGORIES: 'cocktail-categories',
  COCKTAILS: 'cocktails',
  COCKTAILS_IMAGES: 'cocktails_images',
  INGRIDIENTS_CATEGORIES: 'ingridients-categories',
  INGRIDIENTS: 'ingridients',
  INGRIDIENTS_IMAGES: 'ingridients_images',
};
