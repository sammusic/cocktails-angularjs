var User = require('../../models/user');
var UserBar = require('../../models/userBar');

var userBar = {
  list: function (req, res) {
    var offset = req.query.offset || 0;
    var limit = req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');
    var selected = req.query.selected || [];

    var query = UserBar.find({"user": req.user._id});
      query.where('title', regex);
      query.skip(offset);
      query.limit(limit);

      query.exec(function (err, data) {
        return res.json({data: data});
      });
  },

  get: function(req, res){
    var id = req.params.id || '';
    var userId = req.user._id || '';
    if(!id) return res.status(400).send({"message": "Bad request."});

    UserBar.findOne({"_id": id, "user": userId}, function(err, data) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      UserBar.populate(data, 'ingridients', function(err, data){
        return res.status(200).send({data: data});
      });
    });
  },

  create: function(req, res){
    var bar = new UserBar({
        user: req.user._id,
        title: req.body.title,
        description: req.body.description,
        ingridients: req.body.ingridients
    });

    bar.save(function(err, data){
      if(err) return res.status(400).send({'message': err.toString()});
      UserBar.populate(data, 'ingridients', function (err, data) {
        return res.status(200).send({data: data});
      });
    });
  },

  edit: function (req, res) {
    var id = req.params.id || '';
    var userId = req.user._id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    UserBar.findOne({"_id": id, "user": userId}, function(err, bar) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      if(req.body.title){
        bar.title = req.body.title;
      }
      if(req.body.description){
        bar.description = req.body.description;
      }
      bar.ingridients = req.body.ingridients;
      bar.save(function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        UserBar.populate(data, 'ingridients', function (err, data) {
          return res.status(200).send({data: data});
        });
      });
    });
  },

  del: function(req, res){
    var id = req.params.id || '';
    var userId = req.user._id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    UserBar.findOne({"_id": id, "user": userId}, function(err, bar) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      bar.remove({"_id": id}, function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        return res.status(200).send({"message": "Bar has been deleted"});
      });
    });
  }
};

module.exports = userBar;
