module.exports = angular.module('components.admin.cocktailCategory', [])
  .controller('cocktailCategoryListController', require('./cocktailCategory-controller.js'))
  .controller('cocktailCategoryViewController', require('./view/cocktailCategory-controller.js'))
  .controller('cocktailCategoryCreateController', require('./create/cocktailCategory-create-controller.js'))
  .controller('cocktailCategoryEditController', require('./edit/cocktailCategory-edit-controller.js'))

  .directive('cocktailCategoryForm', require('./form/cocktailCategory-form-directive.js') )
;
