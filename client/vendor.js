/* jslint node: true*/

'use strict';

require('angular');
require('angular-resource');
require('angular-sanitize');
require('angular-cookies');
require('angular-translate');
require('angular-mocks');
require('angular-bootstrap-npm');
// ui utilits
require('angular-ui-router');
require('ui-router-metatags/dist/ui-router-metatags.min.js');
require('ui-select');
require('underscore/underscore-min.js');

require('ng-file-upload');
require('ng-sortable');
require('ng-infinite-scroll');
// fix some troubles with phantomjs
require('phantomjs-polyfill');


// var io = require('socket.io/node_modules/socket.io-client/socket.io.js');
// var io = require('socket.io-client');
// var socket = io('http://127.0.0.1:9000/socket.io');
// socket.connect();
// socket.on('connect', function(){
//   console.log('connect');
//   socket.emit('ready');
// });
// socket.on('event', function(data){
//   console.log('event');
// });
// socket.on('disconnect', function(){
//   console.log('disconnect');
// });
// socket.emit('ready');
