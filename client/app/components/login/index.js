module.exports = angular.module('components.login', [])
  .controller('loginController', require('./login-controller.js'))
  .config(require('./login-translations'));
