/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['$scope', 'Query', '$stateParams', '$state', function ($scope, Query, $stateParams, $state) {
  $scope.alerts = [];

  $scope.bar = {};

  Query.bar.get({id: $stateParams.id}).$promise.then(function(data){
    $scope.bar = data.data;
    $scope.bar.ingridients.forEach(function(item){
      $scope.filter.ingridients.push(item._id);
    });
    $scope.loadMore();
  });

  $scope.ingridients = [];
  $scope.getIngridients = function(q){
    q = q || '';
    Query.ingridients.get({q: q}).$promise.then(function(data){
      $scope.ingridients = data.data;
    });
  };

  $scope.onChange = function(){
    // clear all and do filter again
    $scope.busy = false;
    $scope.stopLoading = false;
    $scope.limit = 10;
    $scope.offset = 0;
    $scope.cocktails = [];
    $scope.filter.ingridients = [];
    $scope.bar.ingridients.forEach(function(item){
      $scope.filter.ingridients.push(item._id);
    });
    $scope.loadMore();
  };

  $scope.onUpdateBar = function(){
    var arr = [];
    $scope.bar.ingridients.forEach(function(item){
      arr.push(item._id);
    });
    Query.bar.update({id: $stateParams.id}, {
      ingridients: arr
    }).$promise.then(function(data){
      $scope.bar = data.data;
      $scope.alerts = [{type: 'success', message: 'Bar has been updated!'}];
    });
  };

  $scope.onDeleteBar = function(){
    Query.bar.remove({id: $stateParams.id}).$promise.then(function(data){
      $scope.bar = data.data;
      $scope.alerts = [{type: 'success', message: 'Bar has been deleted!'}];
      $state.go('user');
    });
  };

  $scope.busy = false;
  $scope.stopLoading = false;
  $scope.limit = 10;
  $scope.offset = 0;
  $scope.cocktails = [];

  $scope.filter = {
    categories: [],
    ingridients: []
  };

  $scope.loadMore = function(){
    if($scope.busy || $scope.stopLoading) return;
    $scope.busy = true;

    Query.dashboardCocktails
      .get(angular.merge({
        limit: $scope.limit,
        offset: $scope.offset,
      }, $scope.filter))
      .$promise
      .then(function(res){
        if(res.data){
          $scope.offset += res.data.length;
          for (var i = 0, j = res.data.length; i < j; i++) {
              $scope.cocktails.push(res.data[i]);
          }
          if(res.data.length === 0){
            $scope.stopLoading = true;
          }
          $scope.busy = false;
        }
      });
  };

}];
