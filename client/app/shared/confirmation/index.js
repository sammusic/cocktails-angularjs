module.exports = angular.module('shared.confirmation', [])
.directive('confirmModal', require('./confirmation-directive.js'))
.directive('confirmPopupModal', require('./confirmation-popup-directive.js'))