module.exports = angular.module('shared.navigation', [])
  .directive('navLink', require('./navigation-directive.js'))
  .config(require('./nav-translations'));
