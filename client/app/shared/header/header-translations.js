/* jslint node: true */
'use strict';

module.exports = ['$translateProvider', function ($translateProvider) {
  $translateProvider
    .translations('en', require('./translations/en.js'))
    .translations('ua', require('./translations/ua.js'))
    .translations('ru', require('./translations/ru.js'))
    ;
}];
