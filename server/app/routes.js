var auth = require('./rest/auth');

// from user actions
var fakeData = require('./rest/admin/fakeData');

// user section
var user = require('./rest/user');
var userBar = require('./rest/user-section/userBar');
var userFriend = require('./rest/user-section/user-friend');

// public section
var publicDashboard = require('./rest/public/dashboard');
var publicCocktail = require('./rest/public/cocktail');
var publicCocktailCategory = require('./rest/public/cocktailCategory');
var publicIngridient = require('./rest/public/ingridient');

// admin section
var adminUser = require('./rest/admin/adminUser');
var adminCocktail = require('./rest/admin/adminCocktail');
var adminCocktailCategory = require('./rest/admin/adminCocktailCategory');
var adminIngridient = require('./rest/admin/adminIngridient');
var adminIngridientCategory = require('./rest/admin/adminIngridientCategory');
var dbmigrate = require('./rest/dbmigrate/dbmigrate');

// validate section
var validate = require('./rest/validateRequest');
var validateAdmin = require('./rest/validateAdmin');

// options
var options = require('./rest/options/options-middleware');

var apiPath = '/api/';

module.exports = function(app) {
	app
		// ALL FAKE data
		.get(apiPath + 'create-admin', fakeData.createAdmin)
		.get(apiPath + 'create-cocktails', fakeData.fakeData)
		// .get(apiPath + 'test-email', adminUser.emailTest)

    // AUTH SECTION
    .post(	apiPath + 'login', 						auth.login)
    .post(	apiPath + 'forgot-password', 	auth.forgotPassword)
		.post(	apiPath + 'forgot-password/validate/:validatecode', auth.forgotPasswordValidate)
    .get(		apiPath + 'logout', 					auth.logout)
		.post(	apiPath + 'signup/validate/:validatecode', auth.validate)
		.post(	apiPath + 'signup', 					auth.signup)

		// USER ACTIONS
		.get(		apiPath + 'me', validate, auth.me)
		.put(		apiPath + 'me', validate, auth.meUpdate)
		.delete(apiPath + 'me', validate, auth.meDelete)

		// PUBLIC USER ACTIONS
		.get(		apiPath + 'view/user/:id', validate, user.get)

		// USER FRIEND ACTION
		.post(	apiPath + 'me/friend/:id/approve', validate, userFriend.approve)
		.post(	apiPath + 'me/friend/:id/blocked', validate, userFriend.blocked)
		.post(	apiPath + 'me/friend/:id/decline', validate, userFriend.decline)
		.post(	apiPath + 'me/friend/:id', validate, userFriend.create)
		.put(		apiPath + 'me/friend/:id', validate, userFriend.edit)
		.delete(apiPath + 'me/friend/:id', validate, userFriend.delete)

		// USER BAR SECTION
    .get(		apiPath + 'me/bars', 				validate, userBar.list)
		.get(		apiPath + 'me/bar/:id', 		validate, userBar.get)
		.post(	apiPath + 'me/bar', 				validate, userBar.create)
		.put(		apiPath + 'me/bar/:id', 		validate, userBar.edit)
		.delete(apiPath + 'me/bar/:id', 		validate, userBar.del)


  	// PUBLIC SECTION
    .get(		apiPath + 'dashboard/cocktails', 		publicDashboard.dashboard)
		.get(		apiPath + 'cocktail/:id', 					publicCocktail.get)
		.get(		apiPath + 'cocktails-categories', 	options.list, publicCocktailCategory.list)
		.get(		apiPath + 'ingridients', 						options.list, publicIngridient.list)

		// ADMIN USER SECTION
		.get(		apiPath + 'admin/users', 						validateAdmin, options.list, adminUser.users)
		.get(		apiPath + 'admin/user/:id', 				validateAdmin, adminUser.user)
		.get(		apiPath + 'admin/user/:id/forgot-password', 		validateAdmin, adminUser.getForgotPassword)
		.get(		apiPath + 'admin/user/:id/registration', 				validateAdmin, adminUser.getRegistration)
		.post(	apiPath + 'admin/user', 						validateAdmin, adminUser.create)
		.put(		apiPath + 'admin/user/:id', 				validateAdmin, adminUser.update)
		.delete(apiPath + 'admin/user/:id', 				validateAdmin, adminUser.delete)
		.put(		apiPath + 'admin/user/:id/access', 	validateAdmin, adminUser.access)

		// ADMIN COCKTAIL SECTION
    .get(		apiPath + 'admin/cocktails', 				validateAdmin, options.list, adminCocktail.list)
		.delete(apiPath + 'admin/cocktails', 				validateAdmin, adminCocktail.removeAll)
		.get(		apiPath + 'admin/cocktail/:id', 		validateAdmin, adminCocktail.get)
		.post(	apiPath + 'admin/cocktail', 				validateAdmin, adminCocktail.create)
		.put(		apiPath + 'admin/cocktail/:id', 		validateAdmin, adminCocktail.edit)
		.delete(apiPath + 'admin/cocktail/:id', 		validateAdmin, adminCocktail.remove)

		// COCKTAILS CATEGORY SECTION
    .get(		apiPath + 'admin/cocktails-categories', 	validateAdmin, options.list, adminCocktailCategory.list)
		.delete(apiPath + 'admin/cocktails-categories', 	validateAdmin, adminCocktailCategory.removeAll)
		.get(		apiPath + 'admin/cocktail-category/:id', 	validateAdmin, adminCocktailCategory.get)
		.post(	apiPath + 'admin/cocktail-category', 			validateAdmin, adminCocktailCategory.create)
		.put(		apiPath + 'admin/cocktail-category/:id', 	validateAdmin, adminCocktailCategory.edit)
		.delete(apiPath + 'admin/cocktail-category/:id', 	validateAdmin, adminCocktailCategory.remove)

		// INGRIDIENTS SECTION
    .get(		apiPath + 'admin/ingridients', 		validateAdmin, options.list, adminIngridient.list)
    .delete(apiPath + 'admin/ingridients', 		validateAdmin, adminIngridient.removeAll)
		.get(		apiPath + 'admin/ingridient/:id', validateAdmin, adminIngridient.get)
		.post(	apiPath + 'admin/ingridient', 		validateAdmin, adminIngridient.create)
		.put(		apiPath + 'admin/ingridient/:id', validateAdmin, adminIngridient.edit)
		.delete(apiPath + 'admin/ingridient/:id', validateAdmin, adminIngridient.remove)

		// INGRIDIENTS CATEGORY SECTION
    .get(		apiPath + 'admin/ingridients-categories', 	validateAdmin, options.list, adminIngridientCategory.list)
    .delete(apiPath + 'admin/ingridients-categories', 	validateAdmin, adminIngridientCategory.removeAll)
		.get(		apiPath + 'admin/ingridient-category/:id', 	validateAdmin, adminIngridientCategory.get)
		.post(	apiPath + 'admin/ingridient-category', 			validateAdmin, adminIngridientCategory.create)
		.put(		apiPath + 'admin/ingridient-category/:id', 	validateAdmin, adminIngridientCategory.edit)
		.delete(apiPath + 'admin/ingridient-category/:id', 	validateAdmin, adminIngridientCategory.remove)

		// DBMIGRATE
		.post(apiPath + 'dbmigrate', 	validateAdmin, dbmigrate.init)
		.post(apiPath + 'dbimport', 	validateAdmin, dbmigrate.migrate)
  ;
};
