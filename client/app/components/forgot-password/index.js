module.exports = angular.module('components.forgotPassword', [])
  .controller('forgotPasswordController', require('./forgot-password-controller.js'))
  .config(require('./forgot-password-translations'));
