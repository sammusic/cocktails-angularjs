/* jslint node: true*/
var swagger = require("swagger-node-express");
var express  = require('express');

module.exports = function(app){
  swagger.setAppHandler(app);
  // set api info
  swagger.setApiInfo({
    title: "Swagger Cocktail App",
    //description: "",
    contact: "sammusicdnb@gmail.com",
  });

  swagger.setAuthorizations({
    apiKey: {
      type: "apiKey",
      passAs: "header"
    }
  });

  // Configures the app's base path and api version.
  swagger.configureSwaggerPaths("", "api", "")
  swagger.configure("http://localhost:9000", "1.0.0");

  // Serve up swagger ui at /docs via static route
  var docs_handler = express.static(__dirname + '/../swagger-ui/');
  app.get(/^\/docs(\/.*)?$/, function(req, res, next) {
    if (req.url === '/docs') { // express static barfs on root url w/o trailing slash
      res.writeHead(302, { 'Location' : req.url + '/' });
      res.end();
      return;
    }
    // take off leading /docs so that connect locates file correctly
    req.url = req.url.substr('/docs'.length);
    return docs_handler(req, res, next);
  });
}
