/* jslint node: true */
/* global sessionStorage */
'use strict';

module.exports = ['$scope', 'Query', '$stateParams', '$state', '$timeout', function ($scope, Query, $stateParams, $state, $timeout) {

  $scope.form = {
    password: '',
    passwordVerify: ''
  };
  if(!$stateParams.code){
    $scope.message = 'Oops, something wrong!';
    $timeout(function(){
      $state.go('login');
    }, 3000);
  }

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.submit = function(){
    $scope.alerts = [];
    if($scope.form.password.length && $scope.form.password.length > 7 && $scope.form.password === $scope.form.passwordVerify){
      Query.userChangePassword.post({code: $stateParams.code}, $scope.form).$promise.then(function(data){
        $scope.alerts.push({type: 'success', msg: 'Success!'});
      }, function(error){
        $scope.alerts.push({type: 'danger', msg: error.data.message});
      });
    }else{
      $scope.alerts.push({type: 'warning', msg: 'Your password are differents or less 8 characters'});
    }
  };
}];
