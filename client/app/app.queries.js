/* jslint node: true*/
/* global angular, URL, document, Blob */

'use strict';

module.exports = ['$resource', 'Config', 'authToken', function ($resource, Config, authToken) {
  var token = {
    token: authToken()
  };

  return {
    // basic actions with auth
    userLogin: $resource(Config.apiPath + 'login', {}, { post: {method: 'POST'} }),
    userLogout: $resource(Config.apiPath + 'logout', {}, { post: {method: 'POST'} }),
    userForgotPassword: $resource(Config.apiPath + 'forgot-password', {}, { post: {method: 'POST'} }),
    userChangePassword: $resource(Config.apiPath + 'forgot-password/validate/:code', {code: '@code'}, { post: {method: 'POST'} }),
    userSignUp: $resource(Config.apiPath + 'signup', {}, { post: {method: 'POST'} }),
    userSignUpValidate: $resource(Config.apiPath + 'signup/validate/:code', {code: '@code'}, { post: {method: 'POST'} }),

    // user actions
    me: $resource(Config.apiPath + 'me', {}, {
      get: {method: 'GET', params: token},
      update: {method: 'PUT', params: token},
      delete: {method: 'DELETE', params: token},
    }),

    userView: $resource(Config.apiPath + 'view/user/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
    }),

    // userFriends: $resource(Config.apiPath + 'me/friends', {}, {
    //   get: {method: 'GET', params: token},
    // }),
    userFriend: $resource(Config.apiPath + 'me/friend/:id/:action', {id: '@id', action: '@action'}, {
      // get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      delete: {method: 'DELETE', params: token},
      approve:  {method: 'POST', params: angular.merge({action: 'approve'}, token)},
      decline:  {method: 'POST', params: angular.merge({action: 'decline'}, token)},
      blocked:  {method: 'POST', params: angular.merge({action: 'blocked'}, token)},
    }),

    // bars
    bars: $resource(Config.apiPath + 'me/bars', {}, {
      get: {method: 'GET', params: token},
    }),
    bar: $resource(Config.apiPath + 'me/bar/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token}
    }),

    dbmigrate: $resource(Config.apiPath + 'dbmigrate', {}, {
      post: {method: 'POST', params: token, headers: {accept: 'application/zip'}, responseType: 'arraybuffer', cache: false, transformResponse: function(data, headers, status){
        // var zip = null;
        // if (data) {
        //     zip = new Blob([data], {
        //         type: 'application/zip' //or whatever you need, should match the 'accept headers' above
        //     });
        // }
        //
        // //server should sent content-disposition header
        // var fileName = getFileNameFromHeader(headers('content-disposition'));
        // var result = {
        //     blob: zip,
        //     fileName: fileName
        // };

        // return zip;
        if(status == 200){
          var file = new Blob([ data ], {
              type : 'application/zip'
          });
          //trick to download store a file having its URL
          var fileURL = URL.createObjectURL(file);
          var a         = document.createElement('a');
          a.href        = fileURL;
          a.target      = '_blank';
          a.download    = 'dbmigrate.zip';
          document.body.appendChild(a);
          a.click();
        }

      }},
    }),

    // PUBLIC SECTION
    dashboardCocktails: $resource(Config.apiPath + 'dashboard/cocktails', {}, {
      get: {method: 'GET', params: token},
    }),
    cocktail: $resource(Config.apiPath + 'cocktail/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
    }),
    cocktailCategories: $resource(Config.apiPath + 'cocktails-categories', {}, {
      get: {method: 'GET', params: token},
    }),
    cocktailCategory: $resource(Config.apiPath + 'cocktail-category/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
    }),
    ingridients: $resource(Config.apiPath + 'ingridients', {}, {
      get: {method: 'GET', params: token},
    }),
    ingridient: $resource(Config.apiPath + 'ingridient/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
    }),
    ingridientCategories: $resource(Config.apiPath + 'ingridients-categories', {}, {
      get: {method: 'GET', params: token},
    }),
    ingridientCategory: $resource(Config.apiPath + 'ingridient-category/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
    }),

    // ADMIN SECTION
    // admin user actions
    adminUsers: $resource(Config.apiPath + 'admin/users', {}, {
      get: {method: 'GET', params: token},
    }),
    adminUser: $resource(Config.apiPath + 'admin/user/:id/:option', {id: "@id", option: "@option"}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token},
      access: {method: 'PUT', params: angular.merge({option: 'access'}, token)},
      forgotpassword: {method: 'GET', params: angular.merge({option: 'forgot-password'}, token)},
      registration: {method: 'GET', params: angular.merge({option: 'registration'}, token)},
    }),
    // cocktails
    adminCocktails: $resource(Config.apiPath + 'admin/cocktails', {}, {
      get: {method: 'GET', params: token},
      removeAll: {method: 'DELETE', params: token},
    }),
    adminCocktail: $resource(Config.apiPath + 'admin/cocktail/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token}
    }),
    // cocktails categories
    adminCocktailCategories: $resource(Config.apiPath + 'admin/cocktails-categories', {}, {
      get: {method: 'GET', params: token},
      removeAll: {method: 'DELETE', params: token},
    }),
    adminCocktailCategory: $resource(Config.apiPath + 'admin/cocktail-category/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token}
    }),
    // ingridients
    adminIngridients: $resource(Config.apiPath + 'admin/ingridients', {}, {
      get: {method: 'GET', params: token},
      removeAll: {method: 'DELETE', params: token},
    }),
    adminIngridient: $resource(Config.apiPath + 'admin/ingridient/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token}
    }),
    // ingridients categories
    adminIngridientCategories: $resource(Config.apiPath + 'admin/ingridients-categories', {}, {
      get: {method: 'GET', params: token},
      removeAll: {method: 'DELETE', params: token},
    }),
    adminIngridientCategory: $resource(Config.apiPath + 'admin/ingridient-category/:id', {id: '@id'}, {
      get: {method: 'GET', params: token},
      save: {method: 'POST', params: token},
      update: {method: 'PUT', params: token},
      remove: {method: 'DELETE', params: token}
    }),
  };
}];
