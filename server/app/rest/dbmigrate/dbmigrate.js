var Cocktail = require('../../models/cocktail');
var User = require('../../models/user');

var admZip = require('adm-zip');
var async = require('async');

var bcrypt   = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var util = require('util');
var q = require('q');
var path = require('path');


var CONST = require('./constants');
var UsersMigrate = require('./migrates/users-migrate');
var CocktailsMigrate = require('./migrates/cocktails-migrate');
var CocktailsCategoriesMigrate = require('./migrates/cocktails-categories-migrate');
var IngridientsMigrate = require('./migrates/ingridients-migrate');
var IngridientsCategoriesMigrate = require('./migrates/ingridients-categories-migrate');

var App = {
  init: function(req, res){
    var queries = {};
    var query_value = false;
    if(req.body.items.users){
      query_value = true;
      queries[CONST.USERS] = function(callback){ UsersMigrate.get(req, res, callback); };
    }
    if(req.body.items.cocktails){
      query_value = true;
      queries[CONST.COCKTAILS] = function(callback){ CocktailsMigrate.get(req, res, callback); };
      queries[CONST.COCKTAILS_IMAGES] = function(callback){ CocktailsMigrate.getImages(req, res, callback); };
    }
    if(req.body.items.cocktails_categories){
      query_value = true;
      queries[CONST.COCKTAILS_CATEGORIES] = function(callback){ CocktailsCategoriesMigrate.get(req, res, callback); };
    }
    if(req.body.items.ingridients){
      query_value = true;
      queries[CONST.INGRIDIENTS] = function(callback){ IngridientsMigrate.get(req, res, callback); };
      queries[CONST.INGRIDIENTS_IMAGES] = function(callback){ IngridientsMigrate.getImages(req, res, callback); };
    }
    if(req.body.items.ingridients_categories){
      query_value = true;
      queries[CONST.INGRIDIENTS_CATEGORIES] = function(callback){ IngridientsCategoriesMigrate.get(req, res, callback); };
    }

    if(!query_value) return res.status(500).json({message: 'Error'});

    async.series(queries, function(err, results) {
      var zip = new admZip();
      for(var key in results){
        switch(key){
          case CONST.COCKTAILS_IMAGES:
          case CONST.INGRIDIENTS_IMAGES:
            results[key].forEach(function(image){
              zip.addLocalFolder(image);
            });
            // zip.addLocalFolder(image, key);
          break;
          default:
            zip.addFile(key, new Buffer(results[key]) );
        }
      }
      // zip.writeZip('./dbmigrate.zip');
      return res.status(200).send(zip.toBuffer());
    });
  },

  migrate: function(req, res){
    //CHECK FILE
    if(!req.files.file) return res.status(500).json({message: "No Files."});
    if(req.files.file && !req.files.file.length){
      req.files.file = [req.files.file];
    }
    var results = {images: []};
    var zip = new admZip(req.files.file[0].path);
    var zipEntries = zip.getEntries(); // an array of ZipEntry records

    zipEntries.forEach(function(zipEntry) {
      var data = '';
      switch(zipEntry.entryName){
        case CONST.USERS:
          data = JSON.parse(zipEntry.getData().toString('utf8'));
          results[CONST.USERS] = UsersMigrate.apply(data);
          break;
        case CONST.COCKTAILS:
          data = JSON.parse(zipEntry.getData().toString('utf8'));
          results[CONST.COCKTAILS] = CocktailsMigrate.apply(data);
          break;
        case CONST.COCKTAILS_CATEGORIES:
          data = JSON.parse(zipEntry.getData().toString('utf8'));
          results[CONST.COCKTAILS_CATEGORIES] = CocktailsCategoriesMigrate.apply(data);
          break;
        case CONST.INGRIDIENTS:
          data = JSON.parse(zipEntry.getData().toString('utf8'));
          results[CONST.INGRIDIENTS] = IngridientsMigrate.apply(data);
          break;
        case CONST.INGRIDIENTS_CATEGORIES:
          data = JSON.parse(zipEntry.getData().toString('utf8'));
          results[CONST.INGRIDIENTS_CATEGORIES] = IngridientsCategoriesMigrate.apply(data);
          break;
        default:
          results.images.push(zipEntry.entryName);
          zip.extractEntryTo(zipEntry.entryName, __dirname + '../../../../../', true, true);
      }
    });

    return res.status(200).json({
      message: 'Ok',
      results: results
    });
  }
};

module.exports = App;
