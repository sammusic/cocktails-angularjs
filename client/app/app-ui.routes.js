/* jslint node: true*/
/* global angular */
'use strict';

module.exports = ['$stateProvider', '$urlRouterProvider', 'Config', 'USER_ROLES', '$locationProvider',
function($stateProvider, $urlRouterProvider, Config, USER_ROLES, $locationProvider) {

  // $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');

  var resolve = {
    auth: ['AuthResolver', function(AuthResolver) {
      return AuthResolver.resolve();
    }]
  };

  var generalResolve = {
		auth: ['authResolve', function(authResolve){
			return authResolve();
		}]
	};
  // $urlRouterProvider.otherwise('dashboard');
  $urlRouterProvider.otherwise(function ($injector, $location) {
      var $state = $injector.get('$state');
      $state.go('404');
  });

  var dashboard = {
      name: 'dashboard',
      url: '/',
      templateUrl: Config.rootPath + 'components/dashboard/dashboard-view.html',
      controller: 'dashboardController',
      resolve: generalResolve,
      metaTags: {
          prefix: '',
          title: 'Frontpage',
          description: 'This is the frontpage',
          keywords: 'lots of interresting keywords',
          properties: {
              'og:title': 'Frontpage'
          }
      }
  };
  var dashboardCocktailView = {
      name: 'dashboardCocktailView',
      url: '/cocktail/:id/view',
      templateUrl: Config.rootPath + 'components/dashboard/cocktail-view/cocktail-view.html',
      controller: 'dashboardCocktailViewController',
      resolve: generalResolve
  };
  var aboutus = {
      name: 'about-us',
      url: '/about-us',
      templateUrl: Config.rootPath + 'components/about-us/about-us-view.html',
      controller: 'aboutUsController',
      // resolve: generalResolve,
      metaTags: {
          title: 'About Us',
          description: 'This is the about us page',
          keywords: 'about us',
          properties: {
              'og:title': 'About Us'
          }
      }

  };
  // Auth section
  var login = {
      name: 'login',
      url: '/login',
      templateUrl: Config.rootPath + 'components/login/login-view.html',
      controller: 'loginController',
      resolve: generalResolve,
  };
  var forgotPassword = {
      name: 'forgotPassword',
      url: '/forgot-password',
      templateUrl: Config.rootPath + 'components/forgot-password/forgot-password-view.html',
      controller: 'forgotPasswordController',
      resolve: generalResolve,
  };
  var forgotPasswordValidate = {
      name: 'forgotPasswordValidate',
      url: '/recover-password/:code',
      templateUrl: Config.rootPath + 'components/forgot-password-validate/forgot-password-view.html',
      controller: 'forgotPasswordValidateController',
      resolve: generalResolve,
  };
  var signup = {
      name: 'signup',
      url: '/signup',
      templateUrl: Config.rootPath + 'components/signup/signup-view.html',
      controller: 'signupController',
      resolve: generalResolve
  };
  var signupValidate = {
      name: 'signupValidate',
      url: '/signup/validate/:code',
      templateUrl: Config.rootPath + 'components/signup-validate/signup-view.html',
      controller: 'signupValidateController',
      resolve: generalResolve
  };

  // userPublicViewController
  var viewUser = {
      name: 'viewUser',
      url: '/view/user/:id',
      templateUrl: Config.rootPath + 'components/user/user-view/user-view.html',
      controller: 'userPublicViewController',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };

  // user profile section
  var user = {
      name: 'user',
      url: '/user',
      templateUrl: Config.rootPath + 'components/user/home/user-view.html',
      controller: 'userHomeController',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };
  var userMessage = {
      name: 'userMessages',
      url: '/user/messages',
      templateUrl: Config.rootPath + 'components/user/messages/user-messages-view.html',
      controller: 'userSettingsController',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.admin
      }
  };
  var userSettings = {
      name: 'userSettings',
      url: '/user/settings',
      templateUrl: Config.rootPath + 'components/user/settings/user-settings-view.html',
      controller: 'userSettingsController',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };

  var userBarView = {
      name: 'userBarView',
      url: '/user/bar/:id/view',
      templateUrl: Config.rootPath + 'components/user/bar/view/user-bar-view.html',
      controller: 'userBarControllerView',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };

  var userBarCreate = {
      name: 'userBarCreate',
      url: '/user/bar/create',
      templateUrl: Config.rootPath + 'components/user/bar/create/user-bar-view.html',
      controller: 'userBarControllerCreate',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };
  var userBarEdit = {
      name: 'userBarEdit',
      url: '/user/bar/:id/edit',
      templateUrl: Config.rootPath + 'components/user/bar/edit/user-bar-view.html',
      controller: 'userBarControllerEdit',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.user
      }
  };

  var adminDbMigrate = {
    name: 'adminDbMigrate',
    url: '/admin/dbmigrate',
    templateUrl: Config.rootPath + 'components/admin/dbmigrate/dbmigrate-view.html',
    controller: 'dbmigrateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  var adminUsers = {
      name: 'adminUsers',
      url: '/admin/users',
      templateUrl: Config.rootPath + 'components/admin/users/users-view.html',
      controller: 'adminUsersController',
      resolve: generalResolve,
      data: {
         authorizedRoles: USER_ROLES.admin
      }
  };
  var adminUserView = {
    name: 'adminUserView',
    url: '/admin/user/:id/view',
    templateUrl: Config.rootPath + 'components/admin/users/user-view/user-view.html',
    controller: 'adminUserViewController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // cocktail create/edit section
  var adminUserCreate = {
    name: 'adminUserCreate',
    url: '/admin/user/create',
    templateUrl: Config.rootPath + 'components/admin/users/user-create/user-create-view.html',
    controller: 'adminUserCreateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminUserEdit = {
    name: 'adminUserEdit',
    url: '/admin/user/:id/edit',
    templateUrl: Config.rootPath + 'components/admin/users/user-edit/user-edit-view.html',
    controller: 'adminUserEditController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // cocktail view section
  var adminCocktailList = {
    name: 'adminCocktailList',
    url: '/admin/cocktails/list',
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-view.html',
    controller: 'cocktailListController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminCocktailView = {
    name: 'adminCocktailView',
    url: '/admin/cocktail/:id/view',
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-view/cocktail-view.html',
    controller: 'cocktailViewController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // cocktail create/edit section
  var adminCocktailCreate = {
    name: 'adminCocktailCreate',
    url: '/admin/cocktail/create',
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-create/cocktail-create-view.html',
    controller: 'cocktailCreateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminCocktailEdit = {
    name: 'adminCocktailEdit',
    url: '/admin/cocktail/:id/edit',
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-edit/cocktail-edit-view.html',
    controller: 'cocktailEditController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // cocktail view section
  var adminCocktailCategoryList = {
    name: 'adminCocktailCategoryList',
    url: '/admin/cocktail-categories/list',
    templateUrl: Config.rootPath + 'components/admin/cocktailCategory/cocktailCategory-view.html',
    controller: 'cocktailCategoryListController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminCocktailCategoryView = {
    name: 'adminCocktailCategoryView',
    url: '/admin/cocktail-category/:id/view',
    templateUrl: Config.rootPath + 'components/admin/cocktailCategory/view/cocktailCategory-view.html',
    controller: 'cocktailCategoryViewController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  var adminCocktailCategoryCreate = {
    name: 'adminCocktailCategoryCreate',
    url: '/admin/cocktail-category/create',
    templateUrl: Config.rootPath + 'components/admin/cocktailCategory/create/cocktailCategory-create-view.html',
    controller: 'cocktailCategoryCreateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminCocktailCategoryEdit = {
    name: 'adminCocktailCategoryEdit',
    url: '/admin/cocktail-category/:id/edit',
    templateUrl: Config.rootPath + 'components/admin/cocktailCategory/edit/cocktailCategory-edit-view.html',
    controller: 'cocktailCategoryEditController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // ingridient view section
  var adminIngList = {
    name: 'adminIngList',
    url: '/admin/ingridients/list',
    templateUrl: Config.rootPath + 'components/admin/ingridient/ing-view.html',
    controller: 'ingListController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminIngView = {
    name: 'adminIngView',
    url: '/admin/ingridient/:id/view',
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-view/cocktail-view.html',
    controller: 'cocktailViewController',
    resolve: generalResolve
  };

  // cocktail create/edit section
  var adminIngCreate = {
    name: 'adminIngCreate',
    url: '/admin/ingridient/create',
    templateUrl: Config.rootPath + 'components/admin/ingridient/create/ing-create-view.html',
    controller: 'ingCreateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminIngEdit = {
    name: 'adminIngEdit',
    url: '/admin/ingridient/:id/edit',
    templateUrl: Config.rootPath + 'components/admin/ingridient/edit/ing-edit-view.html',
    controller: 'ingEditController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  // cocktail view section
  var adminIngCategoryList = {
    name: 'adminIngCategoryList',
    url: '/admin/ingridients-categories/list',
    templateUrl: Config.rootPath + 'components/admin/ingridientCategory/ingCategory-view.html',
    controller: 'ingCategoryListController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminIngCategoryView = {
    name: 'adminIngCategoryView',
    url: '/admin/ingridient-category/:id/view',
    templateUrl: Config.rootPath + 'components/admin/ingridientCategory/view/ingCategory-view.html',
    controller: 'ingCategoryViewController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  var adminIngCategoryCreate = {
    name: 'adminIngCategoryCreate',
    url: '/admin/ingridient-category/create',
    templateUrl: Config.rootPath + 'components/admin/ingridientCategory/create/ingCategory-create-view.html',
    controller: 'ingCategoryCreateController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };
  var adminIngCategoryEdit = {
    name: 'adminIngCategoryEdit',
    url: '/admin/ingridient-category/:id/edit',
    templateUrl: Config.rootPath + 'components/admin/ingridientCategory/edit/ingCategory-edit-view.html',
    controller: 'ingCategoryEditController',
    resolve: generalResolve,
    data: {
       authorizedRoles: USER_ROLES.admin
    }
  };

  $stateProvider
      .state('404', {templateUrl: '404.html'})

      .state(dashboard) // home page
        .state(dashboardCocktailView) // home page cocktail view
      .state(aboutus) // about us

      .state(login) // login page
      .state(forgotPassword) // forgot password
      .state(forgotPasswordValidate) // forgot password
      .state(signup) // signup
      .state(signupValidate) // signup validate

      // viewUser
      .state(viewUser) // view public info of user

      .state(user) // home page
        // .state(userMessage) // there should be messages
        .state(userSettings) // change user details
        .state(userBarView) // change user bars
        .state(userBarCreate) // change user bars
        .state(userBarEdit) // change user bars

      /* --- ADMIN SECTION ---- */
      .state(adminUsers) // FOR ADMIN ONLY
        .state(adminUserView) // view user
        .state(adminUserCreate) // user create
        .state(adminUserEdit) // user edit

      .state(adminDbMigrate) // dbmigrate

      .state(adminCocktailList)  // list of all cocktails
        .state(adminCocktailView) // cocktail view page by id
        .state(adminCocktailCreate) // cocktail create page
        .state(adminCocktailEdit) // cocktail edit page by id

      .state(adminCocktailCategoryList)
        .state(adminCocktailCategoryView) // cocktail category view page by id
        .state(adminCocktailCategoryCreate) // cocktail category create page
        .state(adminCocktailCategoryEdit) // cocktail category edit page by id

      .state(adminIngList)  // list of all ingridients
        .state(adminIngView) // ingridient view page by id
        .state(adminIngCreate) // ingridient create page
        .state(adminIngEdit) // ingridient edit page by id

      .state(adminIngCategoryList)
        .state(adminIngCategoryView) // ingridient category view page by id
        .state(adminIngCategoryCreate) // ingridient category create page
        .state(adminIngCategoryEdit) // ingridient category edit page by id
      ;
}];
