/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', '$rootScope', function ($scope, Query, $rootScope) {
  $scope.user = $rootScope.user || {};
  $scope.bars = [];
  Query.bars.get({}).$promise.then(function(data){
    $scope.bars = data.data;
  });
}];
