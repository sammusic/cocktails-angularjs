// load the things we need
var mongoose = require('mongoose');

var forgotPasswordSchema = mongoose.Schema({
  user           : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  code           : String,
  createdAt      : {type: Date, default: Date.now},
  counter        : {type: Number, default: 0},
});

module.exports = mongoose.model('UserForgotPassword', forgotPasswordSchema);
