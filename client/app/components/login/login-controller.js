/* jslint node: true */
/* global sessionStorage */
'use strict';

module.exports = ['$scope', 'Query', '$rootScope', '$state', '$cookies',
function ($scope, Query, $root, $state, $cookies) {

  $scope.$on('$viewContentLoaded', function(event){
    $scope.htmlReady();
  });

  $scope.form = {
    email: '',
    password: ''
  };

  $scope.alerts = [];
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.submit = function(){
    Query.userLogin.post($scope.form).$promise.then(function(res){
      $root.user = res.data;
      if(res.data){
        $cookies.put('authToken', res.data.token);
        $state.go('adminUsers');
        sessionStorage.setItem('user', JSON.stringify(res.data.user));
      }
    }, function(err){
      if($scope.alerts.length){
        $scope.alerts[0].msg = err.data.message;
      }else{
        $scope.alerts.push({type: 'danger', msg: err.data.message});
      }
    });
  };
}];
