var mongoose      = require('mongoose');
var mongooseI18n = require('mongoose-i18n-localize');
var config = require('../../config/database.js');

// define the schema for our cocktail category model
var cocktailCategorySchema = mongoose.Schema({
    title           : { type: String, required: true, i18n: true},
    description     : { type: String, i18n: true},
});

cocktailCategorySchema.plugin(mongooseI18n, {
	locales: config.locales
});

cocktailCategorySchema.pre('save', function (next) {
  if (this.title.length < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  if (this.title.description < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  next();
});

module.exports = mongoose.model('CocktailCategory', cocktailCategorySchema);
