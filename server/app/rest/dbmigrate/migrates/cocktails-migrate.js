var Cocktail = require('../../../models/cocktail');

var walkDir = require('../tools/walkDir');
var sLocale = require('../../options/locales');

var App = {
  get: function(req, res, next){
    Cocktail.find({}, function(err, users){
      if(err) return res.status(500).json(err);
      next(null, JSON.stringify(users));
    });
  },

  apply: function(data){
    data.forEach(function(item){
      Cocktail.findOne({_id: item._id}, function(err, cocktail){
        if(!cocktail){
          new Cocktail(item).save(function(){

          });
        }else{
          var importedTime = new Date(item.updatedAt);
          var inDBTime = new Date(cocktail.updatedAt);
          if(importedTime > inDBTime){
            // TODO: only replace this params
            // NEED TO BE AUTO see users-migrate
            sLocale.compare(cocktail.title, item.title);
            sLocale.compare(cocktail.description, item.description);

            cocktail.categories = item.categories;
            cocktail.ingridients = item.ingridients;
            cocktail.images = item.images;
            cocktail.author = item.author;
            cocktail.visibility = item.visibility;
            cocktail.updatedAt = new Date();

            cocktail.markModified('title');
            cocktail.markModified('description');
            cocktail.markModified('categories');
            cocktail.markModified('ingridients');
            cocktail.markModified('images');
            cocktail.markModified('updatedAt');

            cocktail.save();
          }
        }
      });
    });
  },

  getImages: function(req, res, next){
    var files = walkDir('./uploads/cocktails');
    next(null, files);
  },
};

module.exports = App;
