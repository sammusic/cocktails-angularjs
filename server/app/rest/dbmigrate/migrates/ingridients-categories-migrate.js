var IngridientCategory = require('../../../models/ingridientCategory');

var sLocale = require('../../options/locales');

var App = {
  get: function(req, res, next){
    IngridientCategory.find({}, function(err, data){
      if(err) return res.status(500).json(err);
      next(null, JSON.stringify(data));
    });
  },

  apply: function(data){
    data.forEach(function(item){
      IngridientCategory.findOne({_id: item._id}, function(err, data){
        if(!data){
          new IngridientCategory(item).save();
        }else{
          var importedTime = new Date(item.updatedAt);
          var inDBTime = new Date(data.updatedAt);
          if(importedTime > inDBTime){
            // TODO: only replace this params
            // NEED TO BE AUTO see users-migrate

            sLocale.compare(data.title, item.title);
            sLocale.compare(data.description, item.description);
            data.updatedAt = new Date();

            cocktail.markModified('title');
            cocktail.markModified('description');
            cocktail.markModified('updatedAt');
            data.save();
          }
        }
      });
    });
  },
};

module.exports = App;
