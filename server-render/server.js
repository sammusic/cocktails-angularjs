var cors  = require('cors');

var express = require('express.io');
var app = express();
app.http().io();
var path     = require("path");
var port     = process.env.PORT || 9000;
var mongoose = require('mongoose');
var configDB = require('../server/config/database.js');

var morgan       = require('morgan'); // logger
var bodyParser   = require('body-parser');
var multipart = require('connect-multiparty');
// var fs = require('fs');
// var multipartMiddleware = multipart();

// configuration ===============================================================
app.use(cors());
mongoose.connect(configDB.url); // connect to our database
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multipart());

app.use(express.static(path.join(__dirname, '../client/')));

// routes ======================================================================
require('../server/app/routes.js')(app); // load our routes and pass in our app and fully configured passport
require('../server/config/swagger-init')(app); // init swagger

// app.io.on('connection', function(socket) {
//   console.log('-------------aaaaaaaaaaaaa-----------------');
// });
// app.io.route('ready', function(req) {
//   console.log('---------1---------');
// });
// console.log('---------test---------');

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
