module.exports = angular.module('shared.header', [])
  .directive('appHeader', require('./header-directive.js'))
  .config(require('./header-translations'));
