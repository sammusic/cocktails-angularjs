/* jslint node: true */
'use strict';

module.exports = ['Config', '$rootScope', function (Config, $root) {

  function link(scope) {
    scope.user = $root.user;
    scope.join = formatDate($root.user.user.local.createdAt);
    scope.lastLogin = formatDate($root.user.user.local.lastLoginAt);

    scope.friendLegend = "White approved, Blue waiting, Yellow request, Red blocked";

    function formatDate(date){
      if(date){
        date = new Date(date);
        var monthes = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        return (date.getDay() > 9 ? date.getDay() : '0' + date.getDay())
          + ' ' + monthes[date.getMonth()] + ' ' + date.getFullYear() + ' '
          + (date.getHours() > 9 ? date.getHours() : '0' + date.getHours())
          + ':'
          + (date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes());
      }else{
        return '-';
      }
    }
  }

  return {
    templateUrl: Config.rootPath + 'components/user/sidebar/sidebar-profile/sidebar-profile-view.html',
    link: link,
    replace: true
  };
}];
