/* jslint node: true*/
'use strict';

module.exports = ['$scope', 'Query', '$state', function ($scope, Query, $state) {

  $scope.$on('$viewContentLoaded', function(event){
    $scope.htmlReady();
  });

  $scope.form = {
    firstName: '',
    lastName: '',
    email: '',
    email2: '',
    password: '',
    password2: ''
  };

  $scope.hideBtn = false;

  $scope.errors = {
    email: {
      className: '',
      messages: []
    },
    email2: {
      className: '',
      messages: []
    },
    password: {
      className: '',
      messages: []
    },
    password2: {
      className: '',
      messages: []
    }
  };

  $scope.closeAlert = function(index){
    $scope.alerts.splice(index, 1);
  };

  $scope.onCheckEmail = function(){
    $scope.errors.email.className = '';
    $scope.errors.email.messages = [];
    $scope.errors.email2.className = '';
    $scope.errors.email2.messages = [];
    $scope.hideBtn = false;
    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/i;

    if(!pattern.test($scope.form.email)){
      $scope.errors.email.className = 'has-error';
      $scope.errors.email.messages.push('This isn\'t email address.');
      $scope.hideBtn = true;
    }

    if($scope.form.email != $scope.form.email2){
      $scope.errors.email.className = 'has-error';
      $scope.errors.email.messages.push('Emails aren\t the same.');
      $scope.errors.email2.className = 'has-error';
      $scope.errors.email2.messages.push('Emails aren\t the same.');
      $scope.hideBtn = true;
    }
  };

  $scope.onCheckPass = function(){
    $scope.errors.password.className = '';
    $scope.errors.password.messages = [];
    $scope.errors.password2.className = '';
    $scope.errors.password2.messages = [];
    $scope.hideBtn = false;

    if($scope.form.email.length < 8){
      $scope.errors.password.className = 'has-error';
      $scope.errors.password.messages.push('Password is too short');
      $scope.hideBtn = true;
    }

    if($scope.form.password != $scope.form.password2){
      $scope.errors.password.className = 'has-error';
      $scope.errors.password.messages.push('Passwords are different.');
      $scope.errors.password2.className = 'has-error';
      $scope.errors.password2.messages.push('Passwords are different.');
      $scope.hideBtn = true;
    }
  };
  $scope.submit = function(){
    $scope.form.url = window.location.host;
    Query.userSignUp.post($scope.form).$promise.then(function(data){
      $scope.form.password = $scope.form.password2 = '';
      $scope.alerts = [];
      $scope.alerts.push({type: 'success', message: 'We are sent email to your <' + $scope.form.email + '>. Please check it!'});
      // $state.go('login');
    }, function(err){
      $scope.alerts = [];
      $scope.alerts.push({type: 'danger', message: err.data.message});
    });
  };
}];
