var jwt = require('jwt-simple');
var User = require('../models/user');
var bcrypt   = require('bcrypt-nodejs');

module.exports = function(req, res, next) {
  var token = //(req.body && req.body.token) ||
    (req.query && req.query.token)
    || req.headers['x-access-token'];

  if (token) {
    try {
      var decoded = jwt.decode(token, require('../../config/secret')());

      if (decoded.exp <= Date.now()) {
        res.status(400);
        res.send({"message": "You have expired session, please login again"});
        return;
      }

      // Authorize the user to see if s/he can access our resources
      User.findOne({_id: decoded._id, "local.role": 'admin'}, function(err, user){
        if(err) return res.status(404).send({"message": "Invalid User", "error": err.toString()});

        if(user){
          User.populate(user, [{path: "friends.id", model: "User"}], function(err, user){
            req.user = user;
            req.token = token;
            req.exp = decoded.exp;
            next();
          });
        }else{
          res.status(401);
          res.send({"message": "Invalid User"});
          return;
        }
      });

    } catch (err) {
      res.status(500);
      res.send({"message": "Oops something went wrong", "error": err});
      return ;
    }
  } else {
    res.status(401);
    res.send({"message": "Invalid Token"});
    return;
  }
};
