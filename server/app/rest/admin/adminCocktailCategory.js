var CocktailCategory = require('../../models/cocktailCategory');
var sLocale = require('../options/locales');

var cocktailCategory = {
  list: function (req, res) {
    var offset = req.query.offset || 0;
    var limit = req.query.limit || 10;

    var text = req.query.q || '';
    var regex = new RegExp(text, 'i');

    var query = CocktailCategory.find({});
    query.where({$or: sLocale.createSearchModel([], ['title'], regex)});
    query.skip(+offset);
    query.limit(+limit);

    query.exec(function (err, data) {
      res.json({data: data});
    });
  },

  get: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request."});

    CocktailCategory.findOne({"_id": id}, function(err, data) {
      if(err) return res.status(500).send({"message": "Something wrong"});
      return res.status(200).send({data: data});
    });
  },

  create: function(req, res){
    var category = new CocktailCategory({
        title: req.body.title,
        description: req.body.description,
        createdAt: new Date()
    });

    category.save(function(err, data){
      if(err) return res.status(400).send({'message': err.toString()});
      return res.status(201).send({'data': data, '_id': data._id});
    });
  },

  edit: function (req, res) {
    var id = req.params.id || '';

    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    CocktailCategory.findOne({"_id": id}, function(err, category) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      sLocale.compare(category.title, req.body.title);
      sLocale.compare(category.description, req.body.description);
      category.updatedAt = new Date();

      category.markModified('title');
      category.markModified('description');
      category.markModified('updatedAt');
      category.save(function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        return res.status(200).send({data: data});
      });
    });
  },

  remove: function(req, res){
    var id = req.params.id || '';
    if(!id) return res.status(400).send({"message": "Bad request. No id parameter."});

    CocktailCategory.findOne({"_id": id}, function(err, category) {
      if(err) return res.status(500).send({"message": "Something wrong"});

      category.remove({"_id": id}, function(err, data){
        if(err) return res.status(500).send({'message': err.toString()});
        return res.status(200).send({"message": "category has been deleted"});
      });
    });
  },

  removeAll: function(req, res){
    CocktailCategory.count({}, function(err, count){
      if(err) return res.status(500).send(err);
      CocktailCategory.remove({}, function(err){
        if(err) return res.status(500).send(err);
        res.status(200).json({message: count + ' cocktail categories has been removed'});
      });
    });
  }

};


module.exports = cocktailCategory;
