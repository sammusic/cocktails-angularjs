var easyimg = require('easyimage');
var async = require('async');
var mkdirp = require('mkdirp');
var changeCase = require('change-case');
var path = require('path');
var fs = require('fs');

var globalOp = {
  width: 600,
  height: 600
};
var thumbsOp = {
  width: 200,
  height: 200
};

var ImageModule = {
  remove: function(req, newPath, callback){
    // TODO: remove all images
    
    callback && callback();
  },

  images: function(req, newPath, callback){
    var images = [];

    // small hack for antiarray
    if(req.files.file && !req.files.file.length){
      req.files.file = [req.files.file];
    }

    mkdirp(newPath, function(err) {
      async.forEach(req.files.file, function(file, next){
        var tempPath = file.path;

        var filename = changeCase.lowerCase(file.name);
        var fileext = filename.split('.').pop();
        filename = filename.slice(0, filename.length - fileext.length - 1);

        var targetPath = path.resolve(newPath + filename + "-original" + "." + fileext);
        var readStream = fs.createReadStream(tempPath);
        var writeStream = fs.createWriteStream(targetPath);

        readStream.pipe(writeStream).on("end", function() {
           // Operation done
           return next();
        });

        var objImg = {
          original: newPath + filename + "-original" + "." + fileext,
        };

        // TODO: rebuild
        ImageModule.global(filename, fileext, targetPath, newPath, function(image){
          objImg.global = image.path;

          ImageModule.thumb100(filename, fileext, targetPath, newPath, function(image){
            objImg.thumb100 = image.path;

            ImageModule.thumb200(filename, fileext, targetPath, newPath, function(image){
              objImg.thumb200 = image.path;

              images.push(objImg);
              return next();
            });
          });
        });

      }, function(){
        callback && callback(images);
      });
    });
  },

  global: function(filename, fileext, src, dst, callback){
    easyimg.resize({
       src: src,
       dst: dst + filename + '/' + filename + '-global.' + fileext,
       width: globalOp.width,
       height: globalOp.height,
      }).then(
        function(image) {
          callback && callback(image);
        },
        function (err) {
          callback && callback(err);
        }
      );
  },

  thumb100: function(filename, fileext, src, dst, callback){
    easyimg.resize({
      src: src,
      dst: dst + filename + '/' + filename + '-thumbnail-100.' + fileext,
      width: 100,
      height: 100,
    }).then(
      function (image) {
        callback && callback(image);
      },
      function (err) {
        callback && callback(err);
      }
    );
  },

  thumb200: function(filename, fileext, src, dst, callback){
    easyimg.resize({
      src: src,
      dst: dst + filename + '/' + filename + '-thumbnail-200.' + fileext,
      width: 200,
      height: 200,
    }).then(
      function (image) {
        callback && callback(image);
      },
      function (err) {
        callback && callback(err);
      }
    );
  },

};

module.exports = ImageModule;
