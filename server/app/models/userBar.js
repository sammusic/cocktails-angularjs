// load the things we need
var mongoose = require('mongoose');

// define the schema for our user model
var userBarSchema = mongoose.Schema({
  user           : { type: mongoose.Schema.Types.ObjectId, ref: 'User', require: true},
  title          : { type: String, require: true},
  description    : String,
  ingridients    : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Ingridient'}],
  createdAt      : {type: Date, default: Date.now },
  updatedAt      : {type: Date},
});

// create the model for users and expose it to our app
module.exports = mongoose.model('UserBar', userBarSchema);
