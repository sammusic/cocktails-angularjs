/* jslint node: true*/
/* global angular */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', 'fileReader', 'Upload', 'authToken', '$state', 'tModel',
function (Config, Query, $rootScope, fileReader, Upload, authToken, $state, tModel) {

  function link(scope, element, attr) {

    // initial state
    scope.alerts = [];
    scope.form = tModel.updateModel({});
    scope.title = 'Create new category for ingridient';

    if(attr.id){
      scope.id = attr.id;
      Query.adminIngridientCategory.get({id: scope.id}).$promise.then(function(data){
        scope.form = angular.merge(scope.form, data.data);
        scope.title = 'Edit ' + scope.form.title[$rootScope.lang] + ' ingridient category';
      });
    }

    // submit form
		scope.submit = function(){
      scope.form = tModel.updateModel(scope.form);
      if(scope.id){
        Query.adminIngridientCategory.update({id: scope.id}, scope.form).$promise.then(function(data){
          scope.title = 'Edit ' + data.data.title[$rootScope.lang] + ' ingridient';
          scope.alerts = [{"type": "success", "message": "Ingridient category has been updated."}];
        }, function(err){
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }else{
        Query.adminIngridientCategory.save(scope.form).$promise.then(function(data){
          scope.id = data._id;
          scope.title = 'Edit ' + scope.form.title[$rootScope.lang] + ' ingridient category';
          scope.alerts = [{"type": "success", "message": "New ingridient category has been created."}];
          $state.go('adminIngCategoryEdit', {id: scope.id});
        }, function(err){
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        });
      }
		};

    // remove alert
    scope.closeAlert = function(index){
      scope.alerts.splice(index, 1);
    };
  }

  return {
    templateUrl: Config.rootPath + 'components/admin/ingridientCategory/form/ingCategory-form-view.html',
    link: link,
    replace: true
  };
}];
