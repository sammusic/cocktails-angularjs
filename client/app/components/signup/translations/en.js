
module.exports = {
  SIGNUPFORM: 'Sign Up',
  FIRSTNAMEFIELD: 'First name',
  LASTNAMEFIELD: 'Last name',
  EMAILFIELD: 'Email',
  EMAILVERIFYFIELD: 'Email Verify',
  PASSWORDFIELD: 'Password',
  PASSWORDVERIFYFIELD: 'Password Verify',
  SAVEBUTTON: 'Save',
  RESETBUTTON: 'Reset',
};
