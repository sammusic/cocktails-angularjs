/* jslint node: true*/
/* global angular */

'use strict';

angular.module('app', [
    // angular modules
    'ngResource',
    'ngSanitize',
    'ngCookies',
    'pascalprecht.translate',

    'ui.router',
    'ui.router.metatags',


    'ui.bootstrap',
    'ui.bootstrap.alert',
    'ui.bootstrap.dropdown',
    'ui.bootstrap.modal',
    'ui.bootstrap.progressbar',
    'ui.bootstrap.pagination',
    'ui.bootstrap.rating',
    'ui.bootstrap.tooltip',

    // additional
    'ui.select',
    'as.sortable',
    'ngFileUpload',
    'infinite-scroll',

    'ng',
    'seo',
    require('./components').name,
    require('./shared').name
])
.config(require('./app-ui.routes'))
// .config(require('./app.interceptor'))
.config(require('./app.metatags')) // metatags
.config(require('./app.translates'))
.service('Query', require('./app.queries'))
.run(['$rootScope', 'AUTH_EVENTS', 'AuthService', 'AuthResolver', '$state', '$translate', '$cookies', 'MetaTags',
function ($rootScope, AUTH_EVENTS, AuthService, AuthResolver, $state, $translate, $cookies, MetaTags) {

  $rootScope.MetaTags = MetaTags;
  $rootScope.lang = $cookies.get('lang') ? $cookies.get('lang') : 'en';

  $rootScope.$on('$stateChangeStart', function (event, next) {
    if(next.data){
      var authorizedRoles = next.data.authorizedRoles;
      if (!AuthService.isAuthorized(authorizedRoles)) {
        event.preventDefault();
        if (AuthService.isAuthenticated()) {
          // user is not allowed
          $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        } else {
          // user is not logged in
          $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
        }
        $state.go('login');
      }
    }
  });
}]);
