/**
 * Confirmation bootstrap modal directive
 * @param  {object} $modal - ui bootstrap modal object
 * @return {object} - directive options
 */
module.exports = ['$modal', function($modal){

	function link(scope, element, attrs) {

        scope.modalInfo = scope.modalOptions || {
            description: 'Are you sure?'
        };

		// Set template for modal
		var template = '<div class="modal-body">'+scope.modalInfo.description+'</div>' +
				'<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button>' +
				'<button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';



		// Controller for modal
		var confirmController = ['$scope', '$modalInstance', function($scope, $modalInstance){

			/**
			 * On ok close modal and fire result function
			 * @return {undefined}
			 */
			$scope.ok = function() {
				$modalInstance.close();
			};

            /**
             * On cancel close modal and fire result function
             * @return {undefined}
             */
            $scope.no = function() {
                $modalInstance.dismiss('cancel');
            };

			/**
			 * On cancel dismiss and hide modal
			 * @return {undefined}
			 */
			$scope.cancel = function() {
				$modalInstance.dismiss('cancel');
			};
		}];

        /**
         * Open modal on click on element and pass result function to it
         */
        var modalInstance = $modal.open({
            backdropClass: 'modal in',
            template: template,
            controller: confirmController
        });
        modalInstance.result.then(function(){
            scope.onOk();
        }, function(){
            scope.onCancel();
        });
	}

	// Directive options
	return {
		scope: {
			onOk: '&',
			onCancel: '&',
            modalOptions: '='
		},
		link: link
	}
}]