/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', 'fileReader', 'Upload', 'authToken', '$state', 'tModel',
function (Config, Query, $rootScope, fileReader, Upload, authToken, $state, tModel) {

  function link(scope, element, attr) {

    // initial state
    scope.alerts = [];
    scope.form = tModel.updateModel({});
    scope.form.ingridients = [{ing: '', description: ''}];
    scope.title = 'Create new cocktail';

    if(attr.id){
      scope.id = attr.id;
      Query.adminCocktail.get({id: scope.id}).$promise.then(function(data){
        scope.form = angular.merge(scope.form, data.data);
        scope.title = 'Edit ' + scope.form.title[$rootScope.lang] + ' cocktail';
      });
    }

    scope.categories = [];
    scope.getCategories = function(q){
      q = q || '';
      Query.adminCocktailCategories.get({q: q}).$promise.then(function(data){
        scope.categories = data.data;
      });
    };

    scope.ingridients = [];
    scope.getIngridients = function(q){
      q = q || '';
      Query.adminIngridients.get({q: q}).$promise.then(function(data){
        scope.ingridients = data.data;
      });
    };

    scope.addIngridient = function(){
      scope.form.ingridients.push({ing: '', description: ''});
    };
    scope.removeIngridient = function(index){
      scope.form.ingridients.splice(index, 1);
    };

    // submit form
		scope.submit = function(){
      scope.form = tModel.updateModel(scope.form);
      scope.form.author = $rootScope.user.user._id;

      var arr = [];
      scope.form.ingridients.forEach(function(item){
        arr.push({
          ing: item.ing._id,
          description: item.description
        });
      });
      scope.form.ingridients = arr;

      if(scope.id){
        Upload.upload({
            url: Config.apiPath + 'admin/cocktail/' + scope.id + '?token='+authToken(),
            method: 'PUT',
            file: scope.files,
            data: scope.form
        }).then(function (res) {
          scope.files = [];
          scope.form = res.data.data;
          scope.title = 'Edit ' + res.data.data.title[$rootScope.lang] + ' cocktail';
          scope.alerts = [{"type": "success", "message": "Cocktail "+ res.data.data.title[$rootScope.lang] +" has been updated."}];
        }, function (err) {
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
      }else{
        Upload.upload({
          url: Config.apiPath + 'admin/cocktail' + '?token='+authToken(),
          method: 'POST',
          file: scope.files,
          data: scope.form
        }).then(function (res) {
          scope.files = [];
          scope.form = res.data.data;
          scope.id = res.data.data._id;
          scope.alerts = [{"type": "success", "message": "New cocktail " + scope.form.title[$rootScope.lang] + " has been created."}];
          $state.go('adminCocktailEdit', {
            id: res.data.data._id
          });
        }, function (err) {
          scope.alerts = [{"type": "danger", "message": err.data.message}];
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
      }
		};

    // remove alert
    scope.closeAlert = function(index){
      scope.alerts.splice(index, 1);
    };

    scope.removeImage = function(index){
      scope.form.images.splice(index, 1);
    };
    scope.removeFile = function(index){
      scope.files.splice(index, 1);
    };

    scope.dragControlListeners = {
      accept: function (sourceItemHandleScope, destSortableScope) {return true;},
      itemMoved: function (event) {},
      orderChanged: function(event) {},
      clone: false
    };

    scope.onDelete = function(){
      Query.adminCocktail.remove({id: scope.id}).$promise.then(function(){
        $state.go('adminCocktailList');
      });
    };

    scope.uploadFiles = function (files) {
      if(files){
        scope.files = files;
      }
    };
  }

  return {
    templateUrl: Config.rootPath + 'components/admin/cocktail/cocktail-form/cocktail-form-view.html',
    link: link,
    replace: true
  };
}];
