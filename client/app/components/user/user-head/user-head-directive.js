/* jslint node: true */
'use strict';

module.exports = ['Config', '$rootScope', function (Config, $root) {
  function link(scope) {

  }

  return {
    templateUrl: Config.rootPath + 'components/user/user-head/user-head-view.html',
    link: link,
    replace: true
  };
}];
