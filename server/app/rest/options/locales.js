var Config = require('../../../config/database');

module.exports = {
  createSearchModel: function(searchModel, fields, regex){
    fields.forEach(function(field){
      Config.locales.forEach(function(locale){
        var obj = {};
        obj[field + '.' + locale] = regex;
        searchModel.push(obj);
      });
    });
    return searchModel;
  },

  compare: function(field, translate){
    for(var f in field){
      if(translate[f]){
        field[f] = translate[f];
      }
    }
    return field;
  }
};
