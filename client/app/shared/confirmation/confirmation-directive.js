/**
 * Confirmation bootstrap modal directive
 * @param  {object} $modal - ui bootstrap modal object
 * @return {object} - directive options
 */
module.exports = ['$uibModal', function($modal){

	function link(scope, element, attrs) {

		scope.modalOptions = scope.modalOptions ? scope.modalOptions : {};
		scope.modalInfo = angular.extend({
				description: 'Are you sure to remove?',
				btnOkLabel: 'Ok',
				btnCancelLabel: 'Cancel'
		}, scope.modalOptions);

		// Set template for modal
		var template = '<div class="modal-body">' + scope.modalInfo.description + '</div>'
			+ '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">' + scope.modalInfo.btnOkLabel + '</button>'
			+ '<button class="btn btn-default" ng-click="cancel()">' + scope.modalInfo.btnCancelLabel + '</button></div>';

		/**
		 * Open modal on click on element and pass result function to it
		 */
		element.bind('click', function(){
			var modalInstance = $modal.open({
				backdropClass: 'modal in',
				template: template,
				controller: confirmController
			});
			modalInstance.result.then(function(){
				scope.onOk();
			});
		});

		// Controller for modal
		var confirmController = ['$scope', '$uibModalInstance', function($scope, $uibModalInstance){
			$scope.ok = function() {
				$uibModalInstance.close();
			};
			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			};
		}];
	}

	// Directive options
	return {
		scope: {
			onOk: '&',
			confirmModal: '=',
      modalOptions: '='
		},
		link: link
	};
}];
