/* jslint node: true */
/* global angular */
'use strict';

module.exports = ['$scope', 'Query', function ($scope, Query) {

  $scope.$on('$viewContentLoaded', function(event){
    $scope.htmlReady();
  });

  $scope.busy = false;
  $scope.stopLoading = false;
  $scope.limit = 10;
  $scope.offset = 0;
  $scope.cocktails = [];

  $scope.filter = {
    categories: [],
    ingridients: []
  };

  $scope.$on('dashboard-filter', function(event, args) {
    $scope.filter = args;
    $scope.busy = false;
    $scope.stopLoading = false;
    $scope.limit = 10;
    $scope.offset = 0;
    $scope.cocktails = [];
    $scope.loadMode();
  });

  $scope.loadMode = function(){
    if($scope.busy || $scope.stopLoading) return;
    $scope.busy = true;

    Query.dashboardCocktails
      .get(angular.merge({
        limit: $scope.limit,
        offset: $scope.offset,
      }, $scope.filter))
      .$promise
      .then(function(res){
        if(res.data){
          $scope.offset += res.data.length;
          for (var i = 0, j = res.data.length; i < j; i++) {
              $scope.cocktails.push(res.data[i]);
          }
          if(res.data.length === 0){
            $scope.stopLoading = true;
          }
          $scope.busy = false;
        }
      });
  };
}];
