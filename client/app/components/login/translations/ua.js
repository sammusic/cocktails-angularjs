
module.exports = {
  SIGNINFORM: 'Форма логіна',
  EMAILFIELD: 'Пошта',
  PASSWORDFIELD: 'Пароль',
  REMEMBERME: 'Запам\'ятати мене',
  SIGNINBUTTON: 'Увійти',
  FORGOTPASSWORDLINK: 'Я забув(ла) пароль',
};
