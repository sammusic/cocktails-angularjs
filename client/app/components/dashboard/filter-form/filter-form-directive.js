/* jslint node: true */
'use strict';

module.exports = ['Config', 'Query', '$rootScope', function (Config, Query, $rootScope) {
  function link(scope, element, attr) {
    scope.filter = {
      categories: [],
      ingridients: []
    };

    scope.categories = [];
    scope.ingridients = [];

    scope.getCategories = function(q){
      q = q || '';
      Query.cocktailCategories.get({q: q}).$promise.then(function(data){
        scope.categories = data.data;
      });
    };
    scope.getIngridients = function(q){
      q = q || '';
      Query.ingridients.get({q: q}).$promise.then(function(data){
        scope.ingridients = data.data;
      });
    };

    scope.onChange = function(){
      scope.data = scope.filter;
      $rootScope.$broadcast('dashboard-filter', scope.filter);
    };

    scope.clearFilter = function(){
      scope.filter = {
        categories: [],
        ingridients: []
      };
      $rootScope.$broadcast('dashboard-filter', scope.filter);
    };
  }

  return {
    restrict: 'E',
    scope: {
      data: '='
    },
    templateUrl: Config.rootPath + 'components/dashboard/filter-form/filter-form-view.html',
    link: link,
    replace: true
  };
}];
