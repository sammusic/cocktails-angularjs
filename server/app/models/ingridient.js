// load the things we need
var mongoose      = require('mongoose');
var userSchema    = require('./user');
var mongooseI18n = require('mongoose-i18n-localize');
var config = require('../../config/database.js');

// define the schema for our cocktail model
var ingridientSchema = mongoose.Schema({
    title           : { type: String, required: true, i18n: true},
    description     : { type: String, i18n: true},
    categories      : [{ type: mongoose.Schema.Types.ObjectId, ref: 'IngridientCategory'}],
    images          : [],
    author          : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    createdAt       : { type: Date, default: Date.now},
    updatedAt       : { type: Date}
});

ingridientSchema.statics.getPopulatedFields = function(){
  return [
    {
      path: 'categories',
      model: 'IngridientCategory'
    },
    {
      path: 'author',
      model: 'User'
    },
  ];
};

ingridientSchema.plugin(mongooseI18n, {
	locales: config.locales
});

ingridientSchema.pre('save', function (next) {
  if (this.title.length < 3) return next(new Error('There is very small title. Please type more than 3 symbols'));
  next();
});

module.exports = mongoose.model('Ingridient', ingridientSchema);
