module.exports = function(dir) {
  var fs = fs || require('fs');
  var files = fs.readdirSync(dir);
  var filelist = [];
  files.forEach(function(file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist.push(dir + '/' + file);
    }
  });
  return filelist;
};
