/* jslint node: true*/
/* global angular */
'use strict';

module.exports = ['$translateProvider', function($translateProvider) {
  // $translateProvider.useSanitizeValueStrategy('sanitize');
  $translateProvider.preferredLanguage('en');
}];
