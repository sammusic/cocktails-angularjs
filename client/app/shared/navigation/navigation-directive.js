/* jslint node: true */
'use strict';

module.exports = ['Config', '$location', '$state', '$filter', function (Config, $location, $state, $filter) {
  function link(scope) {
    scope.item.active = false;

    /**
     * Check if passed href is a location of current page
     * @param  {String}  href - href of the link
     * @return {Boolean}
     */
    function isActive(href) {
      if(href == 'dashboard' && $state.current.url == '/'){
        return true;
      }
      return '/' + href === $state.current.url;
    }

    /**
     * Assign checked expression to the item active key
     */
    function check() {
      scope.item.active = isActive(scope.item.href);
    }

    // Set watcher onRouteChangeSuccess and destroyer for it
    var destroyWatcher = scope.$on('$stateChangeSuccess', check);

    // Initial check for active link
    check();

    // Clean up
    scope.$on('$destroy', destroyWatcher);
  }

  return {
    templateUrl: Config.rootPath + 'shared/navigation/navigation-view.html',
    link: link,
    replace: true
  };
}];
