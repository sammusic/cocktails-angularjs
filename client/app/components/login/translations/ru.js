
module.exports = {
  SIGNINFORM: 'Форма логина',
  EMAILFIELD: 'Почта',
  PASSWORDFIELD: 'Пароль',
  REMEMBERME: 'Запомнить меня',
  SIGNINBUTTON: 'Войти',
  FORGOTPASSWORDLINK: 'Я забыл(а) пароль',
};
