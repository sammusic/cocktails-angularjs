// load the things we need
var mongoose = require('mongoose');

var registrationSchema = mongoose.Schema({
  user           : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  code           : String,
  createdAt      : {type: Date, default: Date.now}
});

module.exports = mongoose.model('UserRegistrations', registrationSchema);
