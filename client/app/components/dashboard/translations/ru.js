
module.exports = {
  COCKTAILS_NOT_FOUND: 'Коктейли не найдены',
  DASHBOARD_SEARCH_CATEGORIES: 'Поиск по категориям...',
  DASHBOARD_SEARCH_INGRIDIENTS: 'Поиск по ингредиентам...',
  DASHBOARD_SEARCH_BY_TITLE: 'Поиск по названию',
  DASHBOARD_CLEAR: 'Очистить',

  AUTHOR: 'Автор',
  CATEGORIES: 'Категории',
  INGRIDIENTS: 'Ингредиенты'
};
