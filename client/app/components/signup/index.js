module.exports = angular.module('components.signup', [])
  .controller('signupController', require('./signup-controller.js'))
  .config(require('./signup-translations'));
